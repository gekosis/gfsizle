<?php
/**
 * Created by PhpStorm.
 * User: ozgur.kuru
 * Date: 12.6.2015
 * Time: 16:04
 */

namespace stok;


class bayi {
    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function getList($company_id){
        $this->db->table('BAYILER');
        $this->db->select(array('ID','ADI','SEHIR','ADRES','CEP_TEL','TELEFON','ISKONTO'));
        $this->db->where(array('SIRKET_ID'=>$company_id),'');
        $this->db->get();
        while($result=$this->db->result()){
            $data[]=$result;
        }

        return $data;

    }

    public function getInfo($id){
        $this->db->table('BAYILER');
        $this->db->select(array('ID','ADI','SEHIR','ADRES','CEP_TEL','TELEFON','ISKONTO'));
        $this->db->where(array('ID'=>$id),'');
        $this->db->get();
        return $this->db->result();
    }

    public function isExists($bayi_adi){
        $this->db->table('BAYILER');
        $this->db->select(array('ID'));
        $this->db->where(array('ADI'=>$bayi_adi),'');
        $this->db->get();
        $result= $this->db->result();
        if($result){
            return true;
        }else
        {
            return false;
        }
    }
    public function yetkililistesi($id){
        $this->db->table('BAYI_YETKILI');
        $this->db->select(array('ID','TELEFON','GOREVI','AD'));
        $this->db->where(array('ID'=>$id),'');
        $this->db->get();
        while($result=$this->db->result()){
            $data[]=$result;
        }

        return $data;
    }
    public function bayiNew($data){
        $this->db->table('BAYILER');
        $return=$this->db->insert($data['BAYI']);
        if($return){
            $data['YETKILI']['BAYI_ID']=$return;
            $this->db->table('BAYI_YETKILI');
            $result=$this->db->insert($data['YETKILI']);
        }

        if($result){
            echo 'Bayi eklenmiştir';
        }else{
            echo 'Bayi eklenememiştir';
        }

    }

    public function bayiUpdate($data,$bayi_id){
        $this->db->table('BAYILER');
        $this->db->where(array('ID'=>$bayi_id),'');
        $this->db->update($data);
        return $this->db->affectedRows();
    }


}