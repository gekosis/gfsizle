<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paymet
 *
 * @author okuru
 */

namespace stok;

class Payment
{

    private $result;
    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function requestToken($url, $data)
    {
        $params = array('http' => array(
                'method' => 'POST',
                'content' => $data
        ));
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp) {
            throw new Exception("Problem with $url, $php_errormsg");
        }
        $response = @stream_get_contents($fp);
        if ($response === false) {
            throw new Exception("Problem reading data from $url, $php_errormsg");
        }
        $resultJson = json_decode($response);
        $this->result = $resultJson;
    }

    public function getToken()
    {
        return $this->result->transaction_token;
    }

    public function getLicances($type)
    {
        $this->db->table('LISANS');
        $this->db->select(array('ID', 'LISANS_ADI', 'FIYAT', 'SURE'));
        $this->db->where(array('TUR' => $type, 'DURUM' => 'aktif'), 'AND');
        $this->db->get();
        while ($data = $this->db->result()) {
            $output[] = $data;
        }
        return $output;
    }

    public function getLicanceInfo($id)
    {
        $this->db->table('LISANS');
        $this->db->select(array('ID', 'LISANS_ADI', 'FIYAT', 'SURE'));
        $this->db->where(array('DURUM' => 'aktif', 'ID' => $id), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function createPeyment($data)
    {
        $this->db->table('ODEMELER');
        return $this->db->insert($data);
    }

    public function setLicance($data)
    {
        $this->db->table('LISANS_SIRKET');
        return $this->db->insert($data);
    }

    public function getPreviousPayments($company_id)
    {
        $this->db->table('ODEMELER');
        $this->db->select(array('ID', 'TUTAR', 'YONTEM', 'TARIH', 'ONAY_TARIHI', 'DURUM'));
        $this->db->where(array('SIRKET_ID' => $company_id));
        $this->db->get();
        while ($data = $this->db->result()) {
            $output[] = $data;
        }
        return $output;
    }

    public function bankTransferStatement($data)
    {
        $this->db->table('HAVALE_BILDIRIM');
        return $this->db->insert($data);
    }

    public function getPaymentList(){
        $this->db->table('ODEMELER');
        $this->db->select(array('ID','SIRKET_ID','TUTAR','YONTEM','TARIH','ONAY_TARIHI','DURUM'));
        $this->db->get();
        while($result=$this->db->result()){
            $data[]=$result;
        }
        return $data;
    }
}
