<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of staff
 *
 * @author Özgür
 */

namespace stok;

class Staff
{

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function getStaffInfo($staff_identity)
    {
        $this->db->table('KULLANICILAR');
        $this->db->select(array('ID', 'SIRKET_ID', 'ISIM', 'EMAIL', 'KAYIT_TARIHI', 'SON_GIRIS'));
        if (is_numeric($staff_identity)) {
            $this->db->where(array('ID' => $staff_identity),'');
        } else {
            $this->db->where(array('EMAIL' => $staff_identity),'');
        }
        $this->db->get();
        $staff_info = $this->db->result();

        return $staff_info;
    }

    public function isLogin()
    {
        $email = $_SESSION['user_email'];
        $sid = $_SESSION['ses_id'];
        $key = $email . $sid;
        if (isset($_COOKIE['stokizle']) and $_COOKIE['stokizle'] == $key) {
            return true;
        } else {
            return false;
        }
    }

    public function roleFind($user_menu, $role)
    {
        $return = false;
        foreach ($user_menu as $menu) {

            foreach($menu['sub'] as $key=>$value) {

                if ($role == $value['ROUTE']) {
                    $return = true;
                }
            }

            foreach($menu['process'] as $key=>$value){
                if ($role == $value['ROUTE']) {
                    $return = true;
                }
            }
        }

        return $return;
    }

    public function getStaffProfile($staff_id)
    {
        $this->db->table('KULLANICI_PROFILI');
        $this->db->select(array('ID', 'KULLANICI_ID', 'TELEFON', 'POZISYON', 'METIN'));
        $this->db->where(array('KULLANICI_ID' => $staff_id),'');
        $this->db->get();
        $staff_profile = $this->db->result();

        return $staff_profile;
    }

    public function deleteStaff($id,$company_id)
    {
        $this->db->table('KULLANICILAR');
        $this->db->where(array('ID' => $id,'SIRKET_ID'=>$company_id),'AND');
        $this->db->delete();
        return $this->db->affectedRows();
    }

    public function updateStaffInfo($id, $data)
    {
        $this->db->table('KULLANICILAR');
        if (is_numeric($id)) {
            $this->db->where(array('ID' => $id),'');
        } else {
            $this->db->where(array('EMAIL' => $id),'');
        }
        $return = $this->db->update($data);
        return $return;
    }

    public function updateStaffProfile($id, $data)
    {
        $this->db->table('KULLANICI_PROFILI');
        $this->db->where(array('KULLANICI_ID' => $id),'');
        return $this->db->update($data);
    }

    public function newStaffProfile($data)
    {
        $this->db->table('KULLANICI_PROFILI');
        return $this->db->insert($data);
    }

    public function newStaff($data)
    {
        $this->db->table('KULLANICILAR');
        return $this->db->insert($data);
    }

    public function getStaffList($company_id)
    {
        $this->db->table('KULLANICILAR');
        $this->db->select(array('ID', 'ISIM', 'EMAIL', 'KAYIT_TARIHI'));
        $this->db->where(array('SIRKET_ID' => $company_id),'');
        $this->db->get();
        while ($staff = $this->db->result()) {
            $staff_list[] = $staff;
        }

        return $staff_list;
    }

    public function login($email)
    {
        $this->db->table('KULLANICILAR');
        $this->db->select(array('EMAIL','SIFRE'));
        $this->db->where(array('EMAIL' => $email), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function passwordReset($email, $token)
    {
        global $mail;
        $this->db->table('SIFRE_TOKEN');

        $this->db->insert(array('KULLANICI_EMAIL' => $email,
                'TOKEN' => $token,
                'DURUM' => 'yeni'
        ));

        $to['{LOGINNAME}']=$email;
        $to['{TOKEN}']=$token;
        $mail->send('noreply','Şifre Sıfırlama',$to,'sifreyenileme');

        echo 'Şifre sıfırlama yönergeleri mail adresinize gönderilmiştir';
    }

    public function checkToken($token)
    {
        $this->db->table('SIFRE_TOKEN');
        $this->db->select(array('ID', 'KULLANICI_EMAIL'));
        $this->db->where(array('TOKEN' => $token, 'DURUM' => 'yeni'), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function updateTokens($email)
    {
        $this->db->table('SIFRE_TOKEN');
        $this->db->where(array('KULLANICI_EMAIL' => $email), '');
        return $this->db->update(array('DURUM' => 'eski'));
    }



    public function getPermissionList($staff_id)
    {
        $this->db->table('KULLANICI_YETKILERI');
        $this->db->select(array('ID', 'MODULE_ID'));
        $this->db->where(array('KULLANICI_ID' => $staff_id),'');

        $this->db->get();
        while ($permission = $this->db->result()) {
            $permission_list[] = $permission;
        }
        foreach ($permission_list as $permission) {
            $this->db->table('MODULLER');
            $this->db->select(array('ID', 'MODUL_ADI'));
            $this->db->where(array('ID' => $permission['MODULE_ID']),'');
            $this->db->order('SIRALAMA','ASC');
            $this->db->get();
            $module = $this->db->result();
            $module['PERM_ID'] = $permission['ID'];
            $permission_data[] = $module;
        }
        return $permission_data;
    }

    public function deletePermission($permission_id)
    {
        $this->db->table('KULLANICI_YETKILERI');
        $this->db->where(array('ID' => $permission_id),'');
        $this->db->delete();
        $return = $this->db->affectedRows();
        return $return;
    }

    public function newPermission($staff_id, $perm_id)
    {
        $this->db->table('KULLANICI_YETKILERI');
        return $this->db->insert(array('KULLANICI_ID' => $staff_id, 'MODULE_ID' => $perm_id));
    }

    public function checkPermission($staff_id, $perm_id)
    {
        $this->db->table('KULLANICI_YETKILERI');
        $this->db->select(array('ID'));
        $this->db->where(array('KULLANICI_ID' => $staff_id, 'MODULE_ID' => $perm_id), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function checkEmail($email)
    {
        $this->db->table('KULLANICILAR');
        $this->db->select(array('ID'));
        $this->db->where(array('EMAIL' => $email),'');
        $this->db->get();
        $result = $this->db->result();
        if ($result) {
            $output['ok'] = 'false';
            $output['msg'] = 'Bu email daha önce kayıt edilmiştir';
            return false;
        } else {
            $output['ok'] = 'True';
            $output['msg'] = 'Bu email ile kayıt olabilirsiniz';
            return true;
        }
    }

    public function checkPhone($phone)
    {
        $this->db->table('KULLANICI_PROFILI');
        $this->db->select(array('ID'));
        $this->db->where(array('TELEFON' => $phone),'');
        $this->db->get();
        $result = $this->db->result();
        if ($result) {
            $output['ok'] = 'false';
            $output['msg'] = 'Bu telefon ile daha önce kayıt yapılmıştır.';
            return false;
        } else {
            $output['ok'] = 'True';
            $output['msg'] = 'Bu email ile kayıt olabilirsiniz';
            return true;
        }
    }

    public function smsValidate($sms_api, $data)
    {
        global $sms;
        $sms_code = rand(1000, 9999);
        $mesaj = 'Kayıt için gerekli doğrulama kodu: ' . $sms_code;
        $data['SMS_KODU'] = $sms_code;
        $data['DURUM'] = 'gonderildi';
        $this->db->table('SMS_DOGRULAMA');
        $this->db->insert($data);
        $sms->sendSms($sms_api, array('mesaj' => $mesaj, 'receipents' => $data['TELEFON']));
    }

    public function checkSmsValidation($phone, $email, $sms)
    {
        $this->db->table('SMS_DOGRULAMA');
        $this->db->select(array('ID'));
        $this->db->where(array(
            'TELEFON' => $phone,
            'EMAIL' => $email,
            'SMS_KODU' => $sms
            ), 'AND'
        );
        $this->db->limit(1);
        $this->db->order('ID', 'DESC');
        $this->db->get();
        return $this->db->result();
    }

    public function deleteSmsValidation($phone, $email)
    {
        $this->db->table('SMS_DOGRULAMA');

        $this->db->where(array(
            'TELEFON' => $phone,
            'EMAIL' => $email
            ), 'AND'
        );
        $this->db->delete();
    }

    public function getAllStaff()
    {
        $this->db->table('KULLANICILAR');
        $this->db->select(array('ID', 'SIRKET_ID', 'ISIM', 'EMAIL', 'KAYIT_TARIHI'));
        $this->db->get();
        while ($staff = $this->db->result()) {
            $staff_list[] = $staff;
        }
        foreach ($staff_list as $staff) {
            $this->db->table('SIRKET');
            $this->db->select(array('SIRKET_ADI'));
            $this->db->where(array('ID' => $staff['SIRKET_ID']), '');
            $this->db->get();
            $result = $this->db->result();
            $staff_info[$staff['ID']]['staff_info'] = $staff;
            $staff_info[$staff['ID']]['company_name'] = $result['SIRKET_ADI'];
        }
        return $staff_info;
    }
}
