<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reports
 *
 * @author Özgür
 */

namespace stok;

class reports
{

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function getAllStockCount($company_id)
    {
        $this->db->run('SELECT SUM(ADET) as TOTAL, URUN_ID FROM `DEPO_URUN` WHERE SIRKET_ID=' . $company_id . ' GROUP BY URUN_ID ');
        while ($result = $this->db->result()) {

            $results[] = $result;
        }

        foreach ($results as $result) {
            $this->db->table('URUN_KIMLIK');
            $this->db->select(array('ID', 'BARKOD', 'URUN_ADI'));
            $this->db->where(array('ID' => $result['URUN_ID']));
            $this->db->get();
            $stok_kodu = $this->db->result();
            $new_result[] = $result;
            end($new_result);
            $key = key($new_result);

            $new_result[$key]['BARKOD'] = $stok_kodu['BARKOD'];
            $new_result[$key]['URUN_ADI'] = $stok_kodu['URUN_ADI'];
            $new_result[$key]['ID'] = $stok_kodu['ID'];
        }
        return $new_result;
    }


    public function warehouseProcessReport($data)
    {
        global $stockCls;
        global $staff;
        global $warehouseCls;

        $this->db->run('SELECT ID,PERSONEL_ID,SIRKET_ID,URUN_ID,DEPO_ID,ADET,ISLEM_TARIHI  '
            . 'FROM `URUN_ISLEM` WHERE `ISLEM_TARIHI` BETWEEN "' . $data['BASLANGIC'] . '" AND "' . $data['BITIS'] . '"  AND '
            . 'DEPO_ID=' . $data['DEPO']
            . ' AND ISLEM_TURU="' . $data['ISLEM_TURU'] . '" AND SIRKET_ID=' . $data['SIRKET_ID']);

        while ($result = $this->db->result()) {
            $results[] = $result;
        }

        foreach ($results as $result) {
            $personel = $staff->getStaffInfo($result['PERSONEL_ID']);
            $result['PERSONEL_ID'] = $personel['ISIM'];
            $depo = $warehouseCls->getInfo($result['DEPO_ID'], $result['SIRKET_ID']);

            $result['DEPO_ID'] = $depo['DEPO_ADI'];

            $product_data = $stockCls->getProductInfo($result['URUN_ID']);
            $result['URUN_ID'] = $product_data['URUN_ADI'];
            $output[] = $result;
        }

        return $output;
    }

    public function productProcessReport($data)
    {
        global $stockCls;
        global $staff;
        global $warehouseCls;

        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID'));
        if ($data['BARKOD']) {
            $this->db->where(array('BARKOD' => $data['BARKOD']), '');
        }
        $this->db->get();
        $product_info = $this->db->result();

        if ($product_info) {
            if ($data['DEPO_ID']) {
                $warehouse_query = "AND DEPO_ID = " . $data['DEPO_ID'];
            }
            $this->db->run('SELECT ID,PERSONEL_ID,URUN_ID,DEPO_ID,ADET,ISLEM_TARIHI  '
                . 'FROM `URUN_ISLEM` WHERE `ISLEM_TARIHI` BETWEEN "' . $data['BASLANGIC'] . '" AND "' . $data['BITIS'] . '" '
                . 'AND ISLEM_TURU="' . $data['ISLEM_TURU'] . '" AND SIRKET_ID=' . $data['SIRKET_ID'] . ' AND URUN_ID=' . $product_info['ID'] . $warehouse_query);

            while ($result = $this->db->result()) {
                $results[] = $result;
            }
            foreach ($results as $r) {

                foreach ($r as $key => $val) {
                    $results2[$key] = $val;

                    if ($key == 'PERSONEL_ID') {
                        $staff_data = $staff->getStaffInfo($val);
                        $results2[$key] = $staff_data['ISIM'];
                    }

                    if ($key == 'URUN_ID') {
                        $product_data = $stockCls->getProductInfo($val);
                        $results2[$key] = $product_data['URUN_ADI'];
                    }

                    if ($key == 'DEPO_ID') {
                        $warehouse_data = $warehouseCls->getInfo($val, $data['SIRKET_ID']);

                        $results2[$key] = $warehouse_data['DEPO_ADI'];
                    }
                }
                $output[] = $results2;
            }
            return $output;
        } else {
            return false;
        }
    }

    public function getLastSales($day, $company_id)
    {
        $this->db->run('SELECT SUM(ADET) as adet FROM URUN_ISLEM WHERE ISLEM_TARIHI BETWEEN CURDATE() -'
            . ' INTERVAL ' . $day . ' DAY AND CURDATE() AND ISLEM_TURU="cikis" AND SIRKET_ID=' . $company_id);
        return $result = $this->db->result();
    }

    public function getLastImport($day, $company_id)
    {
        $this->db->run('SELECT SUM(ADET) as adet FROM URUN_ISLEM WHERE GIRIS_TARIHI BETWEEN CURDATE() - '
            . 'INTERVAL ' . $day . ' DAY AND CURDATE() AND AND ISLEM_TURU="giris" AND SIRKET_ID=' . $company_id);
        return $result = $this->db->result();
    }

    public function getTopSeller($company_id)
    {
        $this->db->run('SELECT URUN_ID, SUM(ADET) as adet
                        from URUN_ISLEM
                        group by URUN_ID
                        having SUM(ADET) = (select MAX(city_salary)
                            from (select SUM(ADET)  city_salary
                            from URUN_SATIS where ISLEM_TARIHI BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() AND ISLEM_TURU="cikis" AND SIRKET_ID = ' . $company_id . '
                        group by URUN_ID) tab) ');
        return $result = $this->db->result();
    }

    public function outofStockWarning($company_id, $limit)
    {
        $this->db->table('DEPO_URUN');
        $this->db->select(array('DEPO_ID', 'URUN_ID', 'ADET'));
        $this->db->where(array('SIRKET_ID' => $company_id), '');
        $this->db->get();
        while ($data = $this->db->result()) {

            if ($data['ADET'] < $limit) {
                $stocks[] = $data;
            }
        }

        global $warehouseCls;
        foreach ($stocks as $stock) {
            $depo = $warehouseCls->getInfo($stock['DEPO_ID'], $company_id);
            $stock['DEPO_ADI'] = $depo['DEPO_ADI'];
            $data[] = $stock;
        }

        global $stockCls;
        foreach ($data as $stock) {
            $stock_info = $stockCls->getProductInfo($stock['URUN_ID']);
            $stock['URUN_ADI'] = $stock_info['URUN_ADI'];
            $output[] = $stock;
        }

        return $output;
    }

    public function dailyProductionReport($urun_id)
    {

        $this->db->run("SELECT URUN_ADI,ID FROM URUN_KIMLIK");
        while ($result = $this->db->result()) {
            $data[] = $result;
        }

        foreach ($data as $result) {

            $this->db->run("SELECT SUM(ADET) as ADET,ISLEM_TARIHI,URUN_ID FROM `URUN_ISLEM` WHERE SIRKET_ID=1 AND URUN_ID=$result[ID] GROUP BY ISLEM_TARIHI");
            while ($result = $this->db->result()) {
                $sum = $sum + $result['ADET'];
            }
            $sum = 0;


        }
        $this->db->run("SELECT SUM(ADET) as ADET,ISLEM_TARIHI,URUN_ID FROM `URUN_ISLEM` WHERE SIRKET_ID=1 AND URUN_ID=$urun_id GROUP BY ISLEM_TARIHI");
        while ($result = $this->db->result()) {
            $result['URUN_ADI'] = '';
            $data[] = $result;
        }

        foreach ($data as $count) {
            $this->db->table('URUN_KIMLIK');
            $this->db->select(array('URUN_ADI'));
            $this->db->where(array('ID' => $count['URUN_ID']), '');
            $this->db->get();
            $info = $this->db->result();
            $count['URUN_ADI'] = $info['URUN_ADI'];

            $output['rows'][] = array('c' => $count);

        }
        $output['cols'] = array(array("id" => "", "label" => "Adet", "type" => "number"), array("id" => "", "Label" => "Tarih", "type" => "string"), array("id" => "", "Label" => "Ürün Adı", "type" => "string"), array("id" => "", "Label" => "Ürün ID", "type" => "string"));
        return $output;
    }


    public function acceptedOrdersByProduct()
    {
        global $stockCls;
        global $order;


        $urunler = $stockCls->getAllProducts();

        foreach ($urunler as $urun) {
            $counter[$urun['ID']]['STOCK'] = $stockCls->getProductStocks($urun['ID']);
            $counter[$urun['ID']]['URUN_ADI'] = $urun['URUN_ADI'];
        }
        $accepted_orders = $order->getTodayOrders('onaylandi');
        foreach ($accepted_orders as $key => $item) {
            $products = $order->getProductsOfOrders($item['ID']);
            $accepted_orders[$key]['urunler'] = $products;
        }

        foreach ($accepted_orders as $key => $item) {
            foreach ($item['urunler'] as $key2 => $p) {
                $urun_info = $stockCls->getProductInfo($p['URUN_ID']);
                $counter[$p['URUN_ID']]['ONAY_ADET'] = $counter[$p['URUN_ID']]['ONAY_ADET'] + $p['ADET'];
                $counter[$p['URUN_ID']]['URUN_ADI'] = $urun_info['URUN_ADI'];
            }
        }

        $waiting_orders = $order->getTodayOrders('beklemede');
        foreach ($waiting_orders as $key => $item) {
            $products = $order->getProductsOfOrders($item['ID']);
            $waiting_orders[$key]['urunler'] = $products;
        }

        foreach ($waiting_orders as $key => $item) {
            foreach ($item['urunler'] as $key2 => $p) {
                $counter[$p['URUN_ID']]['BEKLEYEN_ADET'] = $counter[$p['URUN_ID']]['BEKLEYEN_ADET'] + $p['ADET'];

            }
        }
        return $counter;
    }

    public function mevcutDepoRapor($warehouse_id){
        global $stockCls;
        global $order;
        $this->db->table('DEPO_URUN');
        $this->db->select(array('ADET','URUN_ID'));
        $this->db->where(array('DEPO_ID'=>$warehouse_id),'');
        $this->db->get();
        while($result=$this->db->result()){
            $output2[]=$result;
        }

        foreach($output2 as $depo_item){
            $urun=$stockCls->getProductInfo($depo_item['URUN_ID']);
            $output[$depo_item['URUN_ID']]['URUN_ADI']=$urun['URUN_ADI'];

            $rezerv=$order->getOrders('beklemede',$warehouse_id);

            foreach($rezerv as $item){
                $order_items = $order->getProductsOfOrders($item['ID']);
                foreach($order_items as $item2){
                    if($item2['URUN_ID']==$depo_item['URUN_ID']) {
                        $urun_rezerv = $urun_rezerv + $item2['ADET'];
                    }
                }

            }

            $output[$depo_item['URUN_ID']]['KULLANILABILIR']=$depo_item['ADET']-$urun_rezerv;
            $output[$depo_item['URUN_ID']]['REZERV']=$urun_rezerv;
            unset($urun_rezerv);
        }


        return $output;
    }

    public function gunlukOnaylanmisSatisOranlari(){
        global $order;
        global $stockCls;
        $siparisler=$order->getTodayOrders('onaylandi');

        foreach($siparisler as $siparis){
            $siparis_detay=$order->getProductsOfOrders($siparis['ID']);
            foreach($siparis_detay as $item) {
                $toplam = $toplam+$item['ADET'];
                $satisoranlari[$item['URUN_ID']]['TOPLAM'] = $satisoranlari[$item['URUN_ID']]['TOPLAM'] + $item['ADET'];
                $urun_adi = $stockCls->getProductInfo($item['URUN_ID']);
                $urun_adi=$urun_adi['URUN_ADI'];
                $satisoranlari[$item['URUN_ID']]['URUN_ADI']=$urun_adi;
            }
        }

        foreach($satisoranlari as $key=>$oran){
            $hesap = (100*$oran['TOPLAM'])/$toplam;
            $satisoranlari[$key]['ORAN']=round($hesap,2);
        }

        return $satisoranlari;
    }

    public function productOranByBayi($bayi_id){
        global $order;
        global $stockCls;

        $orders=$order->getOrderByBayi($bayi_id,'onaylandi');
        foreach($orders as $key=>$item){
            $total=$total+$item['ADET'];

        }

        foreach($orders as $key=>$oran){
            $orders[$key]['ORAN']=round((100*$oran['ADET'])/$total,2);
        }
        return $orders;
    }

    public function productReports($urun_id,$dates){
        global $stockCls;
        if(is_numeric($urun_id)) {
            $query = "SELECT ID,PERSONEL_ID,URUN_ID,CIKIS_DEPO_ID,GIRIS_DEPO_ID,ADET,ISLEM_TURU,DATE_FORMAT(TIMESTAMP, '%Y-%m-%d') as ts
                FROM URUN_ISLEM WHERE ISLEM_TURU ='uretim' AND
                DATE(TIMESTAMP) BETWEEN '$dates[BASLANGIC]' AND '$dates[BITIS]' AND URUN_ID=$urun_id";
        }else{
            $query = "SELECT ID,PERSONEL_ID,URUN_ID,CIKIS_DEPO_ID,GIRIS_DEPO_ID,ADET,ISLEM_TURU,DATE_FORMAT(TIMESTAMP, '%Y-%m-%d') as ts
                FROM URUN_ISLEM WHERE ISLEM_TURU ='uretim' AND
                DATE(TIMESTAMP) BETWEEN '$dates[BASLANGIC]' AND '$dates[BITIS]'";
        }

        $this->db->run($query);
        while($result=$this->db->result()){
            $uretim[]=$result;
        }

        foreach($uretim as  $key=>$item){
            $product_info = $stockCls->getProductInfo($item['URUN_ID']);
            $gunluk[$item['ts']][$item['URUN_ID']]['ADET']=$item['ADET']+$gunluk[$item['ts']][$item['URUN_ID']]['ADET'];
            $gunluk[$item['ts']][$item['URUN_ID']]['URUN_ADI']=$product_info['URUN_ADI'];


        }
        return $gunluk;
    }




}
