<?php

namespace stok;

class Mail
{

    private $mail;

    public function __construct()
    {
        global $mailer;
        $this->mail = $mailer;
        $this->mail->IsSMTP();
        $this->mail->Host = \stok\Setting::SMTP_SERVER;
        $this->mail->CharSet = 'UTF-8';
        $this->mail->SMTPAuth = \stok\Setting::SMTP_AUTH;
        $this->mail->Host = \stok\Setting::SMTP_SERVER;
        $this->mail->Port = \stok\Setting::SMTP_PORT;
        $this->mail->SMTPSecure = \stok\Setting::SMTP_SECURE;
        $this->mail->Username = \stok\Setting::SMTP_USERNAME;
        $this->mail->Password = \stok\Setting::SMTP_PASSWORD;
    }

    public function send($sender, $subject, $data, $tpl)
    {
        error_log(json_encode($data));
        /*
         * $to= array('email'=>bla,'name'=>bla)
         */
        $html_body = file_get_contents(\stok\Setting::BE_TPL_PATH . 'email/' . $tpl . '.html');
        foreach ($data as $key => $value) {
            $html_body = str_replace($key, $value, $html_body);
        }

        $this->mail->SetFrom(\stok\Setting::$SENDERS[$sender]['mail'], \stok\Setting::$SENDERS[$sender]['name']);
        $this->mail->Subject = $subject;

        $this->mail->MsgHTML($html_body);
        $this->mail->AddAddress($data['{LOGINNAME}'], $data['{SCREENNAME}']);
            
        if (!$this->mail->Send()) {
            
            error_log($this->mail->ErrorInfo);
            return false;

        } else {
            return true;
        }

    }
}
