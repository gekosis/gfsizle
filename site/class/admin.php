<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author Özgür
 */

namespace admin;

class admin
{

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function login($email, $pass)
    {
        $this->db->table('YONETIM');
        $this->db->select(array('EMAIL', 'ISIM'));
        $this->db->where(array('EMAIL' => $email, 'SIFRE' => md5($pass)), 'AND');
        $this->db->get();
        $result = $this->db->result();
        if ($result['EMAIL']) {
            $random = generateRandomString();
            $_SESSION['admin_email'] = $result['EMAIL'];
            $_SESSION['ses_id'] = $random;
            $_SESSION['isim'] = $result['ISIM'];
            $ss = $email . $random;
            setcookie('stokizle_a', "$ss");
        }
        header('Location: ' . \stok\Setting::ADMIN_URL);
    }

    public function isLogin()
    {
        $email = $_SESSION['admin_email'];
        $sid = $_SESSION['ses_id'];
        $key = $email . $sid;
        if (isset($_COOKIE['stokizle_a']) and $_COOKIE['stokizle_a'] == $key) {
            return true;
        } else {
            return false;
        }
    }
}

?>
