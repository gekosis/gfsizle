<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of costumer
 *
 * @author Özgür
 */

namespace stok;

class Customer
{

    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function newCostumer($data)
    {
        $this->db->table('sirket');
        return $this->db->insert($data);
    }

    public function login($email, $pass)
    {

        $this->db->run('SELECT email FROM kullanici WHERE email="' . $email . '" and sifre="' . $pass . '"');
        return $this->db->result();
    }

    public function isLogin()
    {
        $email = $_SESSION['user_email'];
        $sid = $_SESSION['ses_id'];
        $key = $email . $sid;
        if (isset($_COOKIE['stokizle']) and $_COOKIE['stokizle'] == $key) {
            return true;
        } else {
            return false;
        }
    }

    public function isAuth($id, $user_id)
    {
//        $this->db->run('SELECT yetkili_kullanici_id as yid FROM sirket WHERE id="' . $id . '"');
//        $result = $this->db->result();
//        if ($result['yid'] == $user_id) {
//            return true;
//        } else {
//            return false;
//        }
        return true;
    }

    public function refreshSession()
    {
        $_SESSION['info'] = json_encode(self::getUserInfo($_SESSION['user_email']));
        $_SESSION['sirket_data'] = self::getCompanyInfo($_SESSION['sirket_id']);
    }

    public function getUserInfo($user_email)
    {
        //deprecated
        $this->db->run('SELECT * FROM kullanici WHERE email="' . $user_email . '"');

        $kullanici_bilgisi = $this->db->result();

        $this->db->run('SELECT sirket_id, yetki FROM sirket_kullanici WHERE kullanici_id=' . $kullanici_bilgisi['id']);

        while ($r = $this->db->result()) {

            $sirketler['id' . $r['sirket_id']] = $r;
        }
        if ($sirketler) {
            $output = array_merge($kullanici_bilgisi, $sirketler);
        } else {
            $output = $kullanici_bilgisi;
        }
        return $output;
    }

    public function getUserInfoById($id)
    {
        $this->db->run('SELECT * FROM kullanici WHERE id="' . $id . '"');
        return $this->db->result();
    }

    public function deleteUser($id)
    {
        //deprecated
        $this->db->table('kullanici');
        $this->db->where(array('id' => $id));
        return $this->db->delete();
    }

    public function updateUserInfo($id, $data)
    {
        //deprecated
        $this->db->table('kullanici');
        $this->db->where(array('id' => $id));
        return $this->db->update($data);
    }

    public function updatePassword($id, $data)
    {
        //deprecated
        $this->db->table('kullanici');
        $this->db->where(array('id' => $id));
        $data['sifreyenile'] = 0;
        return $this->db->update($data);
    }

    public function newUser($data)
    {
        //deprecated
        $this->db->table('kullanici');
        return $this->db->insert($data);
    }

    public function getUserList($sirket_id)
    {
        //deprecated
        $this->db->run('SELECT kullanici_id,yetki FROM sirket_kullanici WHERE sirket_id="' . $sirket_id . '"');
        while ($r = $this->db->result()) {
            $data[] = $r;
        }
        foreach ($data as $kullanici) {
            $sql = 'SELECT id,isim, telefon FROM kullanici WHERE id=' . $kullanici['kullanici_id'];
            $this->db->run($sql);
            $output[$kullanici['kullanici_id']] = $this->db->result();
            $output[$kullanici['kullanici_id']]['yetki'] = $kullanici['yetki'];
        }
        return $output;
    }

    public function newStore($data)
    {
        $this->db->table('depo');
        return $this->db->insert($data);
    }

    public function getStoreList($sirket_id)
    {
        $this->db->run('SELECT * FROM depo WHERE sirket_id="' . $sirket_id . '"');

        while ($r = $this->db->result()) {
            $data[] = $r;
        }
        return $data;
    }

    public function getStoreInfo($id, $sirket_id)
    {
        if ($sirket_id > 0) {
            $sql = 'SELECT * FROM depo WHERE id="' . $id . '" and sirket_id="' . $sirket_id . '"';
        } else {
            $sql = 'SELECT * FROM depo WHERE id=' . $id;
        }
        $this->db->run($sql);
        return $this->db->result();
    }

    public function updateStore($data)
    {
        $this->db->table('depo');
        $this->db->where(array('id' => $data['id']));
        return $this->db->update($data);
    }

    public function deleteStore($id)
    {
        $this->db->table('depo');
        $this->db->where(array('id' => $id));


        return $this->db->delete();
    }

    public function newPass($email, $pass)
    {
        $this->db->table('kullanici');
        $this->db->where(array('email' => $email));
        return $this->db->update(array('sifre' => md5($pass), 'sifreyenile' => 1));
    }

    public function sendEmail($content)
    {
        $to = $content['to'];
        $subject = $content['subject'];
        $message = $content['message'];
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: no-reply@stokizle.com' . "\r\n" .
            'Reply-To: ozgurkuru.net' . "\r\n" .
            'X-Mailer: STOKIZLE/v1';

        mail($to, $subject, $message, $headers);
    }

    public function getCompanyInfo($id)
    {
        //deprecated
        $this->db->run('SELECT * FROM sirket WHERE id="' . $id . '"');
        return $this->db->result();
    }

    public function getUsersOfCompany($id)
    {
        $this->db->run('SELECT kullanici_id,yetki FROM sirket_kullanici WHERE sirket_id=' . $id);
        while ($r = $this->db->result()) {
            $data[] = $r;
        }

        foreach ($data as $kullanici) {
            $sql = 'select * from kullanici where id=' . $kullanici['kullanici_id'];
            $this->db->run($sql);
            $kullanici_listesi[$kullanici['id']] = $this->db->result();
            ;
            $kullanici_listesi[$kullanici['id']]['yetki'] = $kullanici['yetki'];
        }
        return $kullanici_listesi;
    }

    public function getStoresOfCompany($id)
    {
        $this->db->run('SELECT * FROM depo WHERE tur="iç" and sirket_id="' . $id . '"');

        while ($r = $this->db->result()) {
            $data[] = $r;
        }
        return $data;
    }

    public function updateCompanyOptions($id, $data)
    {
        $this->db->table('sirket');
        $this->db->where(array('id' => $id));
        return $this->db->update($data);
    }

    public function updateCompanyInfo($id, $data)
    {
        $this->db->table('sirket');
        $this->db->where(array('id' => $id));
        return $this->db->update($data);
    }

    public function newField($data)
    {
        $this->db->table('urun_alanlari');
        return $this->db->insert($data);
    }

    public function getFields($id)
    {
        $this->db->run('SELECT * FROM urun_alanlari WHERE sirket_id="' . $id . '"');
        while ($r = $this->db->result()) {
            $data[] = $r;
        }
        return $data;
    }

    public function newStock($data)
    {


        $this->db->table('urunler');
        $id = $this->db->insert($data);
        return $id;
    }

    public function barkodCheckUpdate($barkod, $adet)
    {
        $this->db->run('SELECT id,barkod_kodu,adet FROM urunler WHERE barkod_kodu="' . $barkod . '"');
        $result = $this->db->result();
        if ($result['barkod_kodu']) {
            $adet = $result['adet'] + $adet;
            $this->db->table('urunler');
            $this->db->where(array('id' => $result['id']));
            return $this->db->update(array('adet' => $adet));
        } else {
            return false;
        }
    }

    public function addField2Stock($data)
    {
        $this->db->table('urun_alan');
        $c = count($data);
        $cc = 0;
        foreach ($data as $val) {
            $result = $this->db->insert($val);
            if ($result > 0) {
                $cc++;
            }
        }
        if ($cc == $c) {
            return true;
        } else {
            return false;
        }
    }

    public function addStok2Store($urun_id, $depo_id)
    {
        $this->db->table('urun_depo');
        return $this->db->insert(array('urun_id' => $urun_id, 'depo_id' => $depo_id));
    }

    public function checkStore($store_id, $company_id)
    {
        $sql = "SELECT sirket_id FROM depo WHERE id=" . $store_id . " and sirket_id=" . $company_id;
        $this->db->run($sql);
        return $this->db->result();
    }

    public function getStockofStore($store_id)
    {
        $this->db->run('SELECT id,urun_adi, depo FROM urunler WHERE depo=' . $store_id . ' LIMIT 500');
        while ($r = $this->db->result()) {
            $urunler[] = $r;
        }
        foreach ($urunler as $urun) {
            $urunlerr[$urun['id']]['urunId'] = $urun['id'];
            $urunlerr[$urun['id']]['urunAdi'] = $urun['urun_adi'];
            $d = array();
            $sql = 'select alan_id, deger from urun_alan where urun_id=' . $urun['id'];
            $this->db->run($sql);
            while ($a = $this->db->result()) {
                $d[] = $a;
            }
            if (!is_null($d)) {
                foreach ($d as $a) {
                    $sql2 = 'select alan_adi from urun_alanlari where id=' . $a['alan_id'];
                    $this->db->run($sql2);
                    $alan_adi = $this->db->result();
                    $alan_adi = $alan_adi['alan_adi'];
                    $urunlerr[$urun['id']]['alanlar'][$alan_adi] = $a['deger'];
                }
            }
        }
        return $urunlerr;
    }

    public function deleteField($field_id, $sirket_id)
    {
        $this->db->table('urun_alanlari');
        $this->db->where(array('sirket_id' => $sirket_id, 'id' => $field_id), 'AND');
        $return = $this->db->delete();
        if ($return > 0) {
            $this->db->table('urun_alan');
            $this->db->where(array('alan_id' => $field_id));
            $return2 = $this->db->delete();
        }
        if ($return2 > 0 or $return > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getFieldInfo($field_id, $sirket_id)
    {
        if ($sirket_id != '') {
            $sql = 'SELECT * FROM urun_alanlari WHERE sirket_id=' . $sirket_id . ' AND id=' . $field_id;
        } else {
            $sql = 'SELECT * FROM urun_alanlari WHERE id=' . $field_id;
        }
        $this->db->run($sql);
        return $this->db->result();
    }

    public function updateField($data, $id, $sirket_id)
    {

        $this->db->table('urun_alanlari');
        $this->db->where(array('id' => $id, 'sirket_id' => $sirket_id), 'AND');
        return $this->db->update($data);
    }

    public function roleFind($user_menu, $role)
    {
        $return = false;
        foreach ($user_menu as $key => $value) {
            if (array_search($role, $value['sub']) or array_search($role, $value['process'])) {
                $return = true;
            }
        }
        return $return;
    }

    public function newCompany($data)
    {
        $this->db->table('sirket');
        return $this->db->insert($data);
    }

    public function emailCheck($email)
    {
        $sql = 'SELECT email FROM kullanici WHERE email="' . $email . '"';
        $this->db->run($sql);
        $result = $this->db->result();
        if ($result) {
            $output['ok'] = 'false';
            $output['msg'] = 'Bu email daha önce kayıt edilmiştir';
            return false;
        } else {
            $output['ok'] = 'True';
            $output['msg'] = 'Bu email ile kayıt olabilirsiniz';
            return true;
        }
    }

    public function getCompanies($user_id)
    {
        $sql = 'SELECT sirket_id FROM sirket_kullanici WHERE kullanici_id="' . $user_id . '"';
        $this->db->run($sql);
        while ($result = $this->db->result()) {
            $sirket_id[] = $result['sirket_id'];
        }
        foreach ($sirket_id as $id) {
            $sql = 'SELECT isim FROM sirket WHERE id="' . $id . '" AND durum="aktif"';
            $this->db->run($sql);
            $sirket_data[$id] = $this->db->result();
        }
        return $sirket_data;
    }

    public function addUserToCompany($data)
    {
        $this->db->table('sirket_kullanici');
        return $this->db->insert($data);
    }

    public function checkUserCompany($kullanici_id, $sirket_id)
    {
        $sql = 'SELECT kullanici_id FROM sirket_kullanici WHERE sirket_id=' .
            $sirket_id . ' and kullanici_id=' . $kullanici_id;
        $this->db->run($sql);
        $id = $this->db->result();
        if ($id['kullanici_id'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateKullaniciYetki($kullanici_id, $sirket_id, $yetki)
    {
        $this->db->table('sirket_kullanici');
        $this->db->where(array('kullanici_id' => $kullanici_id, 'sirket_id' => $sirket_id), 'AND');
        return $this->db->update(array('yetki' => $yetki));
    }
}
