<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of company
 *
 * @author Özgür
 */

namespace stok;

class Company
{

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function getCompanyInfo($id)
    {
        $this->db->table('SIRKET');
        $this->db->select(array('ID', 'SIRKET_ADI', 'SIRKET_ADRESI', 'SEHIR', 'DURUM'));
        $this->db->where(array('ID' => $id));
        $this->db->get();
        return $this->db->result();
    }

    public function getCompanyStaffList($company_id)
    {
        $this->db->table('KULLANICILAR');
        $this->db->select(array('ID', 'ISIM', 'EMAIL', 'KAYIT_TARIHI'));
        $this->db->where(array('SIRKET_ID' => $company_id));
        $this->db->order('ID', 'DESC');
        $this->db->get();
        while ($staff = $this->db->result()) {
            $staffs[] = $staff;
        }
        return $staffs;
    }

    public function updateCompanyInfo($id, $data)
    {
        $this->db->table('SIRKET');
        $this->db->where(array('ID' => $id));
        return $this->db->update($data);
    }

    public function getCompanySetting($id)
    {
        $this->db->table('SIRKET_AYAR');
        $this->db->select(array('SIRKET_ID', 'ANA_DEPO', 'STOK_UYARI_ORANI', 'DUSUK_SATIS_ORANI', '	DUSUK_SATIS_PERIYODU'));
        $this->db->where(array('SIRKET_ID' => $id));
        $this->db->get();
        return $this->db->result();
    }

    public function updateCompanySetting($id, $data)
    {
        $this->db->table('SIRKET_AYAR');
        $this->db->where(array('SIRKET_ID' => $id));
        return $this->db->update($data);
    }

    public function newCompany($data, $staff_id)
    {
        $this->db->table('SIRKET');
        $new_company_id = $this->db->insert($data);
        if ($new_company_id > 0) {
            $this->db->table('KULLANICILAR');
            $this->db->where(array('ID' => $staff_id));
            $result = $this->db->update(array('SIRKET_ID' => $new_company_id));
        }
        $default_config = array(
            'SIRKET_ID' => $new_company_id,
            'ANA_DEPO' => 0,
            'STOK_UYARI_ORANI' => 0,
            'DUSUK_SATIS_ORANI' => 0,
            'DUSUK_SATIS_PERIYODU' => 'AYLIK',
        );
        $this->db->table('SIRKET_AYAR');
        $result = $this->db->insert($default_config);

        return $new_company_id;
    }

    public function getPermissions()
    {
        $this->db->table('MODULLER');
        $this->db->select(array('ID'));
        $this->db->get();
        while ($permission = $this->db->result()) {
            $permissions[] = $permission;
        }
        return $permissions;
    }



    public function getCompanies()
    {
        $this->db->table('SIRKET');
        $this->db->select(array('ID', 'SIRKET_ADI', 'SEHIR', 'DURUM','OLUSTURMA_TARIHI'));

        $this->db->get();
        while ($company = $this->db->result()) {
            $companies[] = $company;
        }
        return $companies;
    }
}
