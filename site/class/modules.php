<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modules
 *
 * @author Özgür
 */

namespace stok;

class Modules
{

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function getMenu($id)
    {
        $yetkiler = self::userModules($id);
        $this->db->table('MODULLER');
        $this->db->select(array('ID', 'MODUL_ADI', 'ROUTE', 'TUR', 'UST_MODUL_ID','SIRALAMA'));
        $this->db->where(array('UST_MODUL_ID' => 0), '');
        $this->db->order('SIRALAMA', 'ASC');
        $this->db->get();


        while ($module = $this->db->result()) {
            $masters[] = $module;

        }

        foreach ($masters as $master) {
            $menu[$master['ID']]['MODUL_ADI']=$master['MODUL_ADI'];
            $this->db->table('MODULLER');
            $this->db->select(array('ID', 'MODUL_ADI', 'ROUTE', 'TUR', 'UST_MODUL_ID','SIRALAMA'));
            $this->db->where(array('UST_MODUL_ID'=>$master['ID']),'');
            $this->db->order('SIRALAMA', 'ASC');
            $this->db->get();
            while ($sub = $this->db->result()) {
                $sub['ROUTE']='/'.$master['ROUTE'].'/'.$sub['ROUTE'];
                if($sub['TUR']=='islem') {

                    $menu[$master['ID']]['process'][] = $sub;
                }

                if($sub['TUR']=='sayfa') {
                    $menu[$master['ID']]['sub'][] = $sub;
                }



            }
        }
        return $menu;
    }

    private function userModules($id)
    {
        $this->db->table('KULLANICI_YETKILERI');
        $this->db->select(array('MODULE_ID'));
        $this->db->where(array('KULLANICI_ID' => $id), '');
        $this->db->get();
        while ($yetki = $this->db->result()) {
            $yetkiler[$yetki['MODULE_ID']] = true;
        }

        return $yetkiler;
    }

    public function getModuleList()
    {
        $this->db->table('MODULLER');
        $this->db->select(array('ID', 'MODUL_ADI'));
        $this->db->get();
        while ($module = $this->db->result()) {
            $modules[] = $module;
        }
        return $modules;
    }
}
