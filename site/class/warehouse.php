<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of warehouse
 *
 * @author Özgür
 */

namespace stok;

class warehouse
{

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function getInfo($id)
    {
        $this->db->table('BIRIM');
        $this->db->select(array('ID', 'BIRIM_ADI', 'BIRIM_ADRESI', 'DURUM','TUR'));
        $this->db->where(array('ID' => $id), '');
        $this->db->get();
        return $this->db->result();
    }

    public function getWarehouses($company_id)
    {
        $this->db->table('BIRIM');
        $this->db->select(array('ID', 'BIRIM_ADI', 'BIRIM_ADRESI', 'DURUM','TUR'));
        $this->db->where(array('SIRKET_ID' => $company_id), '');
        $this->db->get();

        while ($warehouse = $this->db->result()) {
            $warehouses[] = $warehouse;
        }
        return $warehouses;
    }

    public function newWarehouse($data)
    {
        $this->db->table('BIRIM');
        return $this->db->insert($data);
    }

    public function getWarehouseStockCount($warehouse_id)
    {
        $this->db->table('DEPO_URUN');
        $this->db->select(array('SUM(ADET) as TOTAL'));
        $this->db->where(array('DEPO_ID' => $warehouse_id), '');
        $this->db->get();
        return $this->db->result();
    }

    public function getStock($warehouse_id)
    {
        $this->db->table('DEPO_URUN');
        $this->db->select(array('URUN_ID', 'ADET'));
        $this->db->where(array('DEPO_ID' => $warehouse_id), '');
        $this->db->get();
        while ($product = $this->db->result()) {
            $products[] = $product;
        }

        foreach ($products as $product) {
            $this->db->table('URUN_KIMLIK');
            $this->db->select(array('URUN_ADI', 'STOK_KODU', 'BARKOD'));
            $this->db->where(array('ID' => $product['URUN_ID']));
            $this->db->get();
            $info = $this->db->result();
            $info['ADET'] = $product['ADET'];
            $product_list[] = $info;
        }
        return $product_list;
    }

    public function checkStock($warehouse_id, $barcode)
    {
        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID'));
        $this->db->where(array('BARKOD' => $barcode));
        $this->db->get();
        $product_id = $this->db->result();

        $this->db->table('DEPO_URUN');
        $this->db->select(array('ID', 'DEPO_ID', 'URUN_ID', 'ADET'));
        $this->db->where(array('DEPO_ID' => $warehouse_id, 'URUN_ID' => $product_id['ID']), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function updateStock($warehouse_id, $product_id, $count)
    {
        $this->db->table('DEPO_URUN');
        $this->db->where(array('DEPO_ID' => $warehouse_id, 'URUN_ID' => $product_id), 'AND');
        return $this->db->update(array('ADET' => $count));
    }

    public function deleteWarehouse($id, $company_id)
    {
        $this->db->table('BIRIM');
        $this->db->where(array('ID' => $id, 'SIRKET_ID' => $company_id), 'AND');
        $this->db->delete();
        $this->db->clean();

        $this->db->table('DEPO_URUN');
        $this->db->where(array('DEPO_ID' => $id, 'SIRKET_ID' => $company_id), 'AND');
        $this->db->delete();
        $this->db->clean();
    }

    public function yetkiliEkle($data){

        $this->db->table('BIRIM_YETKILI');
        $return = $this->db->insert($data);
        if($return){
            echo 'Yetkili Eklenmiştir.';
        }else{
            echo 'İşlem başarısız';
        }
    }

    public function deleteYetkili($id){
        $this->db->table('BIRIM_YETKILI');
        $this->db->where(array('ID'=>$id),'');
       $this->db->delete();
        if($this->db->affectedRows()){
            echo 'Yetkili silinmiştir';
        }else{
            echo 'Yetkili silinme başarısızdır';
        }
    }

    public function update($id,$data){
        $this->db->table('BIRIM');
        $this->db->where(array('ID'=>$id),'');
        $result= $this->db->update($data);
        if($result){
            echo 'Güncelleme başarılı';
        }else{
            echo 'Güncelleme başarısız';
        }
}

    public function yetkiliListesi($birim_id){
        $this->db->table('BIRIM_YETKILI');
        $this->db->select(array('ID','ADI','TELEFON','GOREVI'));
        $this->db->where(array('BIRIM_ID'=>$birim_id),'AND');
        $this->db->get();
        while($result=$this->db->result()){
            $data[]=$result;
        }
        return $data;
    }
}

?>
