<?php
/**
 * Created by PhpStorm.
 * User: ozgur.kuru
 * Date: 22.7.2015
 * Time: 14:27
 */
namespace stok;
class order
{
    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function newOrder($order_data, $urun_data)
    {
        $this->db->table('SIPARIS');
        $order_id = $this->db->insert($order_data);

        foreach ($urun_data as $key => $urun) {
            error_log(json_encode($urun));
            $this->db->table('SIPARIS_URUN');
            $result = $this->db->insert(array('SIPARIS_ID' => $order_id, 'URUN_ID' => $key, 'ADET' => $urun['ADET']));

        }
        $output['order_id']=$order_id;
        $output['result']=$result;
        return $output;
    }

    public function getOrders($status, $depo_id = null)
    {
        $this->db->table('SIPARIS');
        $this->db->select(array('ID', 'BAYI_ID', 'TARIH', 'PERSONEL_ID'));
        if ($depo_id) {
            $this->db->where(array('DURUM' => $status, 'DEPO_ID' => $depo_id), 'AND');
        } else {
            $this->db->where(array('DURUM' => $status), '');
        }

        $this->db->get();
        while ($result = $this->db->result()) {
            $output[] = $result;
        }

        return $output;
    }


    public function getProductsOfOrders($order_id)
    {
        $this->db->table('SIPARIS_URUN');
        $this->db->select(array('ID', 'URUN_ID', 'ADET', 'FIYAT'));
        $this->db->where(array('SIPARIS_ID' => $order_id), '');
        $this->db->get();
        while ($product = $this->db->result()) {
            $output[] = $product;
        }
        return $output;
    }

    public function setOrderStatus($order_id, $status)
    {
        $this->db->table('SIPARIS');
        $this->db->where(array('ID' => $order_id), '');
        $this->db->update(array('DURUM' => $status));
        return $this->db->affectedRows();
    }


    public function getTodayOrders($status)
    {
        $this->db->run("SELECT ID,TARIH,DURUM,DATE_FORMAT(TIMESTAMP, '%Y-%m-%d') FROM SIPARIS WHERE DATE(TIMESTAMP) =CURDATE() AND DURUM='$status'");
        while ($result = $this->db->result()) {
            $output[] = $result;
        }


        return $output;
    }

    public function getOrderDetails($order_id)
    {
        $this->db->table('SIPARIS');
        $this->db->select(array('ID', 'BAYI_ID', 'TARIH', 'PERSONEL_ID', 'DURUM', 'TIMESTAMP', 'DEPO_ID'));
        $this->db->where(array('ID' => $order_id), '');
        $this->db->get();
        $order_data['order_info'] = $this->db->result();

        $order_data['products'] = self::getProductsOfOrders($order_id);

        return $order_data;

    }

    public function getOrderByBayi($bayi_id, $status)
    {
        global $stockCls;
        $this->db->table('SIPARIS');
        $this->db->select(array('ID', 'BAYI_ID', 'TARIH', 'PERSONEL_ID', 'DURUM', 'TIMESTAMP', 'DEPO_ID'));
        $this->db->where(array('BAYI_ID' => $bayi_id, 'DURUM' => $status), 'AND');
        $this->db->get();
        while($result=$this->db->result()){
            $orders[]=$result;
        }

        foreach($orders as $key=>$item){
            $order_products=self::getProductsOfOrders($item['ID']);
            foreach($order_products as $prod_item){
                $urun_info = $stockCls->getProductInfo($prod_item['URUN_ID']);
                $total[$prod_item['URUN_ID']]['ADET']=$prod_item['ADET']+$total[$prod_item['URUN_ID']]['ADET'];
                $total[$prod_item['URUN_ID']]['URUN_ADI']=$urun_info['URUN_ADI'];
            }
        }

        return $total;
    }






}
