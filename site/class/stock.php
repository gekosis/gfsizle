<?php

/**
 * Description of stock
 *
 * @author okuru
 */

namespace stok;

class Stock
{

    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }


    public function getProductId($stokkodu)
    {
        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID'));
        $this->db->where(array('STOK_KODU' => $stokkodu), '');
        $this->db->get();
        $return = $this->db->result();
        return $return['ID'];
    }

    public function getStokKodu($product_id, $company_id)
    {
        $this->db->table("URUN_KIMLIK");
        $this->db->select(array('STOK_KODU'));
        $this->db->where(array("ID" => $product_id, 'SIRKET_ID' => $company_id), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function newProduct($data)
    {
        $this->db->table('URUN_KIMLIK');
        return $this->db->insert($data);
    }

    public function addProductToWarehouse($depo_id, $urun_id, $stock)
    {
        /*
         * DEPO_URUN kaydı eklenmesi
         */
        $this->db->table('DEPO_URUN');
        return $this->db->insert(array('DEPO_ID' => $depo_id, 'URUN_ID' => $urun_id, 'ADET' => $stock));
    }

    public function checkProductWarehouse($product_id, $warehouse_id)
    {
        /*
         * DEPO STOK Kaydı kontrolü
         */
        $this->db->table('DEPO_URUN');
        $this->db->select(array('ID', 'ADET'));
        $this->db->where(array('URUN_ID' => $product_id, 'DEPO_ID' => $warehouse_id), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function updateWarehouseStock($id, $stock)
    {
        /*
         * DEPO_URUN kaydı güncellemesi
         */
        $this->db->table('DEPO_URUN');
        $this->db->where(array('ID' => $id), '');
        return $this->db->update(array('ADET' => $stock));
    }

    public function stockTransfer($data)
    {
        /*
         * STOK KODU'ndan ürün id çekilir.
         * Ürün ID false ise işlem bitirilir.
         */
        $urun_id = self::getProductId($data['STOK_KODU']);
        if (!$urun_id) {
            echo 'Geçersiz bir stok kodu girdiniz.';
            die;
        }

        /*
         * İlgili ürün sayısı çıkış deposundan kontrol edilir.
         */
        $cikis_kontrol = self::checkProductWarehouse($urun_id, $data['CIKIS_DEPO']);

        if ($data['ADET'] > $cikis_kontrol['ADET']) {
            echo 'Belirttiğiniz adet miktarı ilgili depoda bulunmamaktadır. Mevcut Stok Durumu: ' . $cikis_kontrol['ADET'];
            die;
        }

        /*
         * Eğer çıkış deposunda yeteri miktarda ürün varsa ürün çıkış işlemi sağlanır.
         */
        $eksi_stok = $cikis_kontrol['ADET'] - $data['ADET'];
        $eksi_result = self::updateWarehouseStock($cikis_kontrol['ID'], $eksi_stok);

        /*
         * Çıkış depodan adet düşürülme işlemi gerçekleştiyse giriş depoya ürün ekleme işlemi yapılır
         * eğer mevcut depoda ilgili ürüne ait kayıt varsa mevcut kayıt güncellenir, yoksa yeni kayıt açılır.
         */
        if ($eksi_result) {
            $giris_kontrol = self::checkProductWarehouse($urun_id, $data['GIRIS_DEPO']);

            /*
             * eğer giriş yapılacak depoda daha önce aynı ürün kaydı varsa bu kayıt güncellenir.
             * yoksa yeni kayıt açılır.
             */
            if ($giris_kontrol) {
                $stok = $giris_kontrol['ADET'] + $data["ADET"];
                $arti_stok_result = self::updateWarehouseStock($giris_kontrol['ID'], $stok);
            } else {
                $arti_stok_result = self::addProductToWarehouse($giris_kontrol['ID'], $urun_id, $data['ADET']);
            }
        }

        if ($arti_stok_result) {
            global $staff_info;
            $process_array=array(
                'PERSONEL_ID'=>$staff_info->ID,
                'URUN_ID'=>$urun_id,
                'CIKIS_DEPO_ID'=>$data['CIKIS_DEPO'],
                'GIRIS_DEPO_ID'=>$data['GIRIS_DEPO'],
                'ADET'=>$data['ADET'],
                'ISLEM_TURU'=>'Transfer'
            );
            self::newStockProcess($process_array);
            echo 'Stok transfer başarılı olmuştur';
        } else {
            echo 'Stok transfer başarısız olmuştur';
        }

    }

    public function getProductInfo($id)
    {
        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID', 'URUN_ADI', 'STOK_KODU', 'BARKOD', 'SIRKET_ID', 'URUN_KAPASITE', 'PALET_KAPASITE', 'KOLI_KAPASITE', 'DEPOZITO'));
        $this->db->where(array('ID' => $id), '');
        $this->db->get();
        return $this->db->result();
    }

    public function getProductList($company_id)
    {
        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID', 'URUN_ADI', 'STOK_KODU', 'BARKOD', 'SIRKET_ID'));
        $this->db->where(array('SIRKET_ID' => $company_id), '');
        $this->db->get();
        while ($data = $this->db->result()) {

            $products[] = $data;
        }

        foreach ($products as $key => $product) {


            $this->db->table('URUN_FIYAT');
            $this->db->select(array('FIYAT'));
            $this->db->where(array('URUN_ID' => $product['ID']), '');
            $this->db->order('ID', 'DESC');
            $this->db->limit(1);
            $this->db->get();
            $price = $this->db->result();
            $product['price'] = $price['FIYAT'];
            $products[$key] = $product;

        }
        return $products;
    }

    public function getWarehouseStocks($depo_id, $product_id)
    {
        $this->db->table('DEPO_URUN');
        $this->db->select(array('DEPO_ID', 'ADET'));
        $this->db->where(array('DEPO_ID' => $depo_id, 'URUN_ID' => $product_id), 'AND');
        $this->db->get();
        return $this->db->result();
    }

    public function getAllProducts()
    {
        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID', 'URUN_ADI', 'STOK_KODU', 'BARKOD', 'SIRKET_ID'));
        $this->db->get();
        while ($data = $this->db->result()) {
            $products[] = $data;
        }
        return $products;
    }

    public function getDailyProcessCount()
    {
        $date = date('Y-m-d');
        $this->db->run("SELECT COUNT(ID) as count FROM `URUN_ISLEM` WHERE `TIMESTAMP` BETWEEN '$date 00:00:00'
                        AND '$date 23:59:59'");


        $result = $this->db->result();
        return $result['count'];
    }

    public function updateProduct($data)
    {
        $this->db->table('URUN_KIMLIK');
        $this->db->where(array('ID' => $data['ID']), '');
        return $this->db->update($data);
    }

    public function newPrice($data)
    {
        $this->db->table('URUN_FIYAT');
        return $this->db->insert($data);

    }

    public function getPrice($product_id)
    {
        $this->db->table('URUN_FIYAT');
        $this->db->select(array('FIYAT'));
        $this->db->where(array('URUN_ID' => $product_id), '');
        $this->db->order('ID', 'DESC');
        $this->db->limit('1');
        $this->db->get();
        return $this->db->result();
    }

    /**
     * newStock:
     * $data ADET, STOK_KODU, DEPO_ID olarak üç parametre içerir.
     * URUN_KIMLIK tablosundan stok kodu kontrolü yapılır.
     * DEPO_URUN işlemleri gerçekleştirilir
     * STOK_URUN işlemleri gerçekleştirilir.
     * Sonuç olarak mesaj döner.
     */
    public function newStock($data)
    {

        $this->db->table('URUN_KIMLIK');
        $this->db->select(array('ID'));
        $this->db->where(array('STOK_KODU' => $data['STOK_KODU']), '');
        $this->db->get();
        $result = $this->db->result();
        $urun_id = $result['ID'];
        if (!$urun_id) {
            echo 'HATALI ÜRÜN KODU GİRDİNİZ!';
            die;
        }

        /*
         * DEPO_URUN kaydı varsa o kayıt güncellenir, yoksa yeni kayıt açılır
         */
        $this->db->table('DEPO_URUN');
        $this->db->select(array('ID', 'ADET'));
        $this->db->where(array('DEPO_ID' => $data['DEPO_ID'], 'URUN_ID' => $urun_id), 'AND');
        $this->db->get();
        $result = $this->db->result();
        $depo_urun_id = $result['ID'];
        $depo_urun_adet = $result['ADET'];

        if ($depo_urun_id > 0) {
            $adet = $depo_urun_adet + $data['ADET'];
            $this->db->table('DEPO_URUN');
            $this->db->where(array('ID' => $depo_urun_id), '');
            $uru_depo_result = $this->db->update(array('ADET' => $adet));
        } else {
            $this->db->table('DEPO_URUN');
            $uru_depo_result = $this->db->insert(array('URUN_ID' => $urun_id, 'DEPO_ID' => $data['DEPO_ID'], 'ADET' => $data["ADET"]));
        }

        /*
         * DEPO_URUN güncellemesi başarılı olduğu taktirde URUN_STOK tablosu ile ilgili işlemler yapılır.
         * URUN_STOK kaydı varsa mevcut kayıt güncellenir, yoksa yeni kayıt açılır.
         */
        if ($uru_depo_result) {
            $this->db->table('URUN_STOK');
            $this->db->select(array('ID', 'STOK'));
            $this->db->where(array('URUN_ID' => $urun_id), '');
            $this->db->get();
            $result = $this->db->result();
            $urun_stok_id = $result['ID'];
            $stok = $result['STOK'] + $data['ADET'];
            if ($urun_stok_id) {
                $this->db->table('URUN_STOK');
                $this->db->where(array('ID' => $urun_stok_id), '');
                $urun_stok_result = $this->db->update(array('STOK' => $stok));
            } else {
                $this->db->table('URUN_STOK');
                $urun_stok_result = $this->db->insert(array('URUN_ID' => $urun_id, 'STOK' => $data['ADET']));
            }
        }

        if ($urun_stok_result) {
            global $staff_info;
            $process_array=array(
                'PERSONEL_ID'=>$staff_info->ID,
                'URUN_ID'=>$urun_id,
                'CIKIS_DEPO_ID'=>0,
                'GIRIS_DEPO_ID'=>$data['DEPO_ID'],
                'ADET'=>$data['ADET'],
                'ISLEM_TURU'=>'uretim'
            );
            self::newStockProcess($process_array);
            echo 'Stok ekleme tamamlanmıştır';
        } else {
            echo 'Stok ekleme başarısız olmuştur';
        }
    }

    public function getProductStocks($urun_id)
    {
        $this->db->table('URUN_STOK');
        $this->db->select(array('STOK'));
        $this->db->where(array('URUN_ID' => $urun_id), '');
        $this->db->get();
        $return = $this->db->result();
        return $return['STOK'];
    }

    public function dropStock($urun_id, $stock_count)
    {
        $this->db->run("UPDATE URUN_STOK SET STOK=STOK-$stock_count WHERE URUN_ID=$urun_id");
    }

    public function dropWarehouseStock($depo_id, $urun_id, $stock_count)
    {
        $this->db->run("UPDATE DEPO_URUN SET ADET=ADET-$stock_count WHERE URUN_ID=$urun_id AND DEPO_ID=$depo_id");
    }

    public function addStockToBayi($bayi_id, $urun_id, $adet)
    {
        $this->db->table('BAYI_STOK');
        $this->db->select(array('ID'));
        $this->db->where(array('BAYI_ID' => $bayi_id, 'URUN_ID' => $urun_id), 'AND');
        $this->db->get();
        $stok_exists = $this->db->result();
        $record_id = $stok_exists['ID'];
        if (!$stok_exists) {
            $this->db->table('BAYI_STOK');
            $record_id = $this->db->insert(array('BAYI_ID' => $bayi_id, 'URUN_ID' => $urun_id, 'STOK' => '0'));
        }

        $this->db->run('UPDATE BAYI_STOK SET STOK=STOK+'.$adet.' WHERE BAYI_ID='.$bayi_id.' AND URUN_ID='.$urun_id);



    }

    public function newStockProcess($data){
        $this->db->table('URUN_ISLEM');
        return $this->db->insert($data);
    }

}
