<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paket
 *
 * @author okuru
 */

namespace stok;

class Paket
{
    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function paketBilgisi($sirket_id)
    {
        $sql = 'SELECT paket, paket_bitis_tarihi FROM sirket WHERE id=' . $sirket_id;
        $this->db->run($sql);
        $paket_bilgisi = $this->db->result();
        return $paket_bilgisi;
    }

    public function yeniPaket($sirket_id, $paket_adi, $bitis_tarihi)
    {
        $this->db->table('sirket');
        $this->db->where(array('id' => $sirket_id), '');
        return $this->db->update(array('paket' => $paket_adi, 'paket_bitis_tarihi' => $bitis_tarihi));
    }

    public function getPackageList(){
        $this->db->table('LISANS');
        $this->db->select(array('ID','LISANS_ADI','TUR','FIYAT','SURE','DURUM','OLUSTURMA_TARIHI'));
        $this->db->get();
        while($result=$this->db->result()){
            $list[]=$result;
        }
        foreach($list as $package){
            $this->db->run('SELECT COUNT(DISTINCT SIRKET_ID) AS c FROM LISANS_SIRKET WHERE LISANS_ID='.$package[ID]);
            $result=$this->db->result();
            $package['SIRKET_SAYISI']=$result['c'];
            $new_list[]=$package;
        }

        return $new_list;
    }
}
