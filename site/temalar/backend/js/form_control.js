// When the browser is ready...
$(function() {

    // Setup form validation on the #register-form element
    $("#kullanicikayit").validate({
        // Specify the validation rules
        rules: {
            isim: "required",
            telefon: "required",
            email: {
                required: true,
                email: true,
                remote: 'https://stok.stokizle.com/api/mailCheck/'
            },
            sifre: {
                required: true,
                minlength: 5
            }
        },
        // Specify the validation error messages
        messages: {
            telefon: "Bir telefon belirtmeniz gerekmektedir.",
            isim: "İsim Girmeniz Gerekiyor",
            sifre: {
                required: "Şifre girmeniz gerekiyor.",
                minlength: "Minimum 5 karakterli bir şifre belirlemelisiniz"
            },
            email: {
                required: "Mail adresi yazmanız gerekmektedir.",
                email: "Geçerli Bir Email Adresi Giriniz",
                remote: "Bu mail adresi daha önce kayıt edilmiştir.."
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});

// When the browser is ready...
$(function() {

    // Setup form validation on the #register-form element
    $("#stokekle").validate({
        // Specify the validation rules
        rules: {
            urun_adi: "required",
            adet: "required"

        },
        // Specify the validation error messages
        messages: {
            urun_adi: "İsim Girmeniz Gerekiyor",
            adet: "Adet Giriniz",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});

// When the browser is ready...
$(function() {

    // Setup form validation on the #register-form element
    $("#alanekle").validate({
        // Specify the validation rules
        rules: {
            alan_adi: "required",
        },
        // Specify the validation error messages
        messages: {
            alan_adi: "İsim Girmeniz Gerekiyor",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});


$(function() {

    // Setup form validation on the #register-form element
    $("#depoekle").validate({
        // Specify the validation rules
        rules: {
            isim: "required",
        },
        // Specify the validation error messages
        messages: {
            isim: "İsim Girmeniz Gerekiyor",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});





