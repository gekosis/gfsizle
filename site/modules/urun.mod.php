<?php
/**
 * Created by PhpStorm.
 * User: ozgur.kuru
 * Date: 29.7.2015
 * Time: 17:03
 */
namespace modules;

class ModUrun{
    public function __construct()
    {
        global $staff;
        global $company;
        global $is_login;
        global $role;

        $this->is_login=$is_login;
        $this->role=$role;
        $this->staff = $staff;
        $this->company = $company;
    }

    public function kartkaydet(){

        global $staff_info;
        global $stockCls;
        if ($this->is_login and $this->role) {
            foreach ($_POST as $key => $val) {
                $data[$key] = removeXSS($val);
            }
            $fiyat = $data['URUN_FIYATI'];
            unset($data['URUN_FIYATI']);
            $data['SIRKET_ID'] = $staff_info->SIRKET_ID;
            $result = $stockCls->newProduct($data);
            if ($result) {
                $stockCls->newPrice(array('URUN_ID' => $result, 'FIYAT' => $fiyat));
                echo "Stok Kartı Eklendi";
            } else {
                echo "Stok kartı ekleme başarısız oldu";
            }

        } else {
            echo 'Yetkisiz Alan';
        }
    }

    public function liste(){
        global $stockCls;
        global $staff_info;
        global $twig;
        if ($this->is_login and $this->role) {
            $products = $stockCls->getProductList($staff_info->SIRKET_ID);
            echo $twig->render('stok-kart-listesi.html', array('products' => $products));
        }
    }

    public function kartguncelle(){
        global $route;
        global $stockCls;
        global $product_id;
        if ($this->is_login and $this->role) {
            foreach ($_POST as $key => $val) {
                $data[$key] = removeXSS($val);
            }
            $data['ID'] = $route->getId();
            $fiyat = $data['URUN_FIYATI'];
            unset($data['URUN_FIYATI']);
            $result = $stockCls->updateProduct($data);
            if ($result) {
                $price = $stockCls->getPrice($product_id);
                if ($price['FIYAT'] != $fiyat) {
                    $stockCls->newPrice(array('URUN_ID' => $data['ID'], 'FIYAT' => $fiyat));
                }

                echo 'Kart Güncellenmiştir';

            } else {
                echo 'Kart güncelleme başarısız olmuştur';
            }
        } else {
            echo 'Yetkisiz İşlem';
        }
    }

    public function kartduzenle(){
        global $route;
        global $stockCls;
        global $staff_info;
        global $twig;
        if ($this->is_login and $this->role) {
            if ($route->getId()) {
                $product = $stockCls->getProductInfo($route->getId());

                $products = $stockCls->getProductList($staff_info->SIRKET_ID);

                echo $twig->render('stok-kart-listesi.html', array(
                    'products' => $products,
                    'duzenle' => true,
                    'product' => $product,
                    'id', $route->getId()

                ));
            } else {
                header('Location:' . \stok\Setting::BE_URL . '/urun/liste');
            }
        } else {
            header('Location:' . \stok\Setting::BE_URL);
        }
    }
}