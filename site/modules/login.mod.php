<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author okuru
 */

namespace modules;

class ModLogin
{

    public function __construct()
    {
        global $staff;
        global $company;

        $this->staff = $staff;
        $this->company = $company;
    }

    public function login()
    {
        global $is_login;

        if (!$is_login) {
            $email = removeXSS($_POST['login']);
            $hash = $this->staff->login($email);
            if (password_verify(removeXSS($_POST['pass']), $hash['SIFRE'])) {
                $logged_user_email = $hash['EMAIL'];
            }
            if ($logged_user_email) {
                $random = generateRandomString();
                $_SESSION['user_email'] = $logged_user_email;
                $_SESSION['ses_id'] = $random;
                $_SESSION['info'] = json_encode($this->staff->getStaffInfo($_SESSION['user_email']));

                $ss = $logged_user_email . $random;
                $userInfo = json_decode($_SESSION['info']);
                $_SESSION['sirket_id'] = $userInfo->SIRKET_ID;
                $_SESSION['sirket_data'] = $this->company->getCompanyInfo($userInfo->SIRKET_ID);
                $_SESSION['profile'] = json_encode($this->staff->getStaffProfile($userInfo->ID));
                setcookie('stokizle', "$ss");
                $login_date = date('Y-m-d h:i:s');
                 $this->staff->updateStaffInfo($_SESSION['user_email'], array('SON_GIRIS' => $login_date));
                header('Location: ' . \stok\Setting::BE_URL);
            } else {
                header('Location: ' . \stok\Setting::BE_URL . '/?msg=Hatalı Giriş Yaptınız...');
            }
        } else {
            header('Location: ' . \stok\Setting::BE_URL);
        }
    }

    public function logout()
    {
        global $is_login;

        if ($is_login) {

            session_destroy();
            unset($_COOKIE['stokizle']);

            $message = 'Çıkış işleminiz başarılı olmuştur. İyi günler dileriz...';
        } else {
            $message = 'Giriş yapılmamış. İyi günler dileriz...';
        }
        return $message;
    }

    public function refreshSession()
    {
        $_SESSION['info'] = json_encode($this->staff->getStaffInfo($_SESSION['user_email']));
        $_SESSION['sirket_data'] = $this->company->getCompanyInfo($_SESSION['sirket_id']);
    }
}
