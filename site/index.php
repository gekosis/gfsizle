<?php

session_set_cookie_params(60000, "/", "v2.stokizle.com");
session_start();
require_once('/srv/stokizle.com/site/config.php');
require_once(\stok\Setting::FW_PATH . "lib/general.lib.php");
require_once(\stok\Setting::FW_PATH . "lib/memcache.lib.php");
require_once(\stok\Setting::FW_PATH . "lib/mysql.lib.php");
require_once(\stok\Setting::FW_PATH . "lib/routing.lib.php");
require_once(\stok\Setting::FW_PATH . "lib/phpmailer/class.phpmailer.php");
require_once(\stok\Setting::FW_PATH . "lib/sms.lib.php");
require_once(\stok\Setting::FW_PATH . "modules/routing.mod.php");
require_once(\stok\Setting::FW_PATH . "modules/db.mod.php");

require_once(\stok\Setting::SITE_PATH . "class/mail.php");

require_once(\stok\Setting::SITE_PATH . "modules/login.mod.php");
require_once(\stok\Setting::SITE_PATH . "modules/urun.mod.php");

// yeni sistem
require_once(\stok\Setting::SITE_PATH . "class/staff.php");
require_once(\stok\Setting::SITE_PATH . "class/modules.php");
require_once(\stok\Setting::SITE_PATH . "class/company.php");
require_once(\stok\Setting::SITE_PATH . "class/stock.php");
require_once(\stok\Setting::SITE_PATH . "class/warehouse.php");
require_once(\stok\Setting::SITE_PATH . "class/reports.php");
require_once(\stok\Setting::SITE_PATH . "class/bayi.php");
require_once(\stok\Setting::SITE_PATH . "class/order.php");
require_once(\stok\Setting::VENDOR_PATH . "autoload.php");
// onemli tanimlar

$routing = new arte\Routing();
$route = new arte\Routes($routing->router());
$debug = $route->getDebug();

$database = new arte\Database(array(
    'user' => \stok\Setting::MYSQL_USER,
    'password' => \stok\Setting::MYSQL_PASSWORD,
    'host' => \stok\Setting::MYSQL_HOST,
    'database' => \stok\Setting::MYSQL_DATABASE
));

$db = new arte\db();

$mailer = new PHPMailer();
$mail = new \stok\Mail();

$stockCls = new \stok\Stock();

// yeni sistem
$modules = new \stok\Modules();
$staff = new \stok\Staff();
$company = new \stok\Company();
$warehouseCls = new \stok\warehouse();
$sms = new arte\sms();
$rapor = new \stok\reports();
$bayi = new \stok\bayi();
$order = new \stok\order();


switch ($route->getSubdomain()) {
    case 'v2':
        $time = 3;
        $is_login = $staff->isLogin();
        $staff_info = json_decode($_SESSION['info']);
        $staff_profile = json_decode($_SESSION['profile']);
        $loader = new Twig_Loader_Filesystem(\stok\Setting::BE_TPL_PATH);
        $twig = new Twig_Environment($loader, array(
            'cache' => \stok\Setting::CACHE_PATH,
            'debug' => true,
        ));
        $twig->addExtension(new Twig_Extension_Debug());
        $twig->addGlobal('tpl_url', \stok\Setting::BE_TPL_URL);
        $twig->addGlobal('title', 'Stok Yönetim Paneli');
        $twig->addGlobal('version', '2.0 Beta');

        if ($is_login) {


            $twig->addGlobal('staff_info', $staff_info);
            $twig->addGlobal('user_screen_name', $staff_info->ISIM);
            $twig->addGlobal('sirket_data', $_SESSION['sirket_data']);
            $twig->addGlobal('id', $staff_info->ID);
            if ($_SESSION['sirket_id']) {
                $sirket_id = $_SESSION['sirket_id'];
                $obj_key = 'id' . $sirket_id;
                $user_menu = $modules->getMenu($staff_info->ID);
                $role = $staff->roleFind($user_menu, '/' . $route->getMaster() . '/' . $route->getSub());
                $twig->addGlobal('sirket_id', $_SESSION['sirket_id']);
                $twig->addGlobal('last_login', $staff_info->SON_GIRIS);
                $twig->addGlobal('user_menu', $user_menu);

            }

        }
        switch ($route->getMaster()) {
            case 'api':
                require('api.php');
                break;
            case 'giris':
                $modLogin = new \modules\modLogin();
                $modLogin->login();
                unset($modLogin);
                break;
            case 'sifremiunuttum':

                if (!$is_login) {
                    if($_POST){
                        $token = generateRandomString();
                        $staff->passwordReset(removeXss($_POST['login']),$token);
                    }else {
                        echo $twig->render('sifremiunuttum.html');
                    }
                } else {
                    header('Location: ' . \stok\Setting::BE_URL);
                }
                break;
            case 'sifresifirla':
                if(!$is_login) {
                    $token = removeXSS($route->getSub());
                    $check=$staff->checkToken($token);
                    if($check){
                        if ($_GET['o']==1){
                            if(removeXSS($_POST['pass1']) == removeXSS($_POST['pass2'])){
                                $result = $staff->updateStaffInfo($check['KULLANICI_EMAIL'],array('SIFRE'=>password_hash($_POST['pass1'],PASSWORD_DEFAULT)));
                                if($result){

                                    $staff->updateTokens($check['KULLANICI_EMAIL']);
                                    echo 'Şifre sıfırlama başarılı olmuştur';
                                }else{
                                    echo 'İşlem başarısız';
                                }
                            }else{
                                echo 'Şifre kontrol başarısız olmuştur';
                            }
                        }else {
                            echo $twig->render('sifreyeni.html', array('token' => $token));
                        }
                    }else{
                        header('Location: ' . \stok\Setting::BE_URL);
                    }
                }
                break;
            case 'cikis':
                $modLogin = new \modules\modLogin();
                $modLogin->logout();
                header('Location:' . \stok\Setting::BE_URL);
                break;


            // Ürün Modülü
            case 'urun':
                switch ($route->getSub()) {
                    case 'kartkaydet':
                        $modUrun = new \modules\ModUrun();
                        $modUrun->kartkaydet();
                        unset($modUrun);
                        break;
                    case 'liste':
                        $modUrun = new \modules\ModUrun();
                        $modUrun->liste();
                        unset($modUrun);
                        break;
                    case 'kartguncelle':
                        $modUrun = new \modules\ModUrun();
                        $modUrun->kartguncelle();
                        unset($modUrun);
                        break;
                    case 'kartduzenle':
                        $modUrun = new \modules\ModUrun();
                        $modUrun->kartduzenle();
                        unset($modUrun);
                        break;
                    default:
                        header('Location:' . \stok\Setting::BE_URL);
                        break;
                }
                break;
            //Rapor Modülü
            case 'rapor':
                switch ($route->getSub()) {

                    case 'urun':
                        if ($is_login and $role) {
                            if ($_POST) {
                                $baslangic = $_POST['baslangic'];
                                $bitis = $_POST['bitis'];
                                $uretim_raporu = $rapor->productReports(null, array('BASLANGIC' => $baslangic, 'BITIS' => $bitis));

                                $begin = new DateTime($baslangic);
                                $end = new DateTime($bitis);
                                $end = $end->modify('+1 day');

                                $interval = new DateInterval('P1D');
                                $daterange = new DatePeriod($begin, $interval, $end);

                                foreach ($daterange as $date) {
                                    //echo $date->format("Ymd") . "<br>";
                                }


                            }
                            echo $twig->render('rapor-urun.html', array('uretim' => $uretim_raporu));


                        } else {
                            header('Location:' . \stok\Setting::BE_URL);
                        }

                        break;
                    default:
                        /*
                        //$total_product_report = $rapor->getAllStockCount($staff_info->SIRKET_ID);
                        $tendays_sales = $rapor->getLastSales(10, $staff_info->SIRKET_ID);
                        $tendays_imports = $rapor->getLastImport(10, $staff_info->SIRKET_ID);
                        $top_seller = $rapor->getTopSeller($staff_info->SIRKET_ID);
                        $top_seller_info = $stockCls->getProductInfo($top_seller['URUN_ID']);
                        $top_seller['URUN_ADI'] = $top_seller_info['URUN_ADI'];
                        $tpl->assign('tendays_sales', $tendays_sales);
                        $tpl->assign('tendays_imports', $tendays_imports);
                        $tpl->assign('top_seller', $top_seller);
                        $tpl->assign('product_list', $total_product_report);
                        echo $tpl->draw('report-general', true);
                        */
                        break;
                }

                break;
            // Stok Modülü
            case 'stok':
                switch ($route->getSub()) {
                    case 'ekle':
                        if ($is_login and $role) {

                            $depo_check = count($warehouseCls->getWarehouses($staff_info->SIRKET_ID));
                            if ($depo_check > 0) {
                                $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                                $html = $twig->render('stok-ekle.html', array(
                                    'warehouses' => $warehouses,
                                ));
                            } else {
                                $html = $twig->render('warehouse.html', array(
                                    'msg' => '<p><b>Hiç deponuz bulunmuyor. Lütfen Depo Ekleyin</b></p>',
                                ));
                            }
                            echo $html;


                        } else {
                            header('Location: ' . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'transfer':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }
                            $stockCls->stockTransfer($data);
                        } else {
                            echo 'Yetkisiz Giriş';
                        }
                        break;
                    case 'kaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }
                            $stockCls->newStock($data);

                        } else {
                            echo 'Yetkisiz Giriş';
                        }
                        break;
                    default:
                        header('Location:' . \stok\Setting::BE_URL);
                        break;
                }
                break;
            // Satış Modülü
            case 'satis':
                switch ($route->getSub()) {
                    case 'yeni':
                        if ($is_login and $role) {
                            $products = $stockCls->getProductList($staff_info->SIRKET_ID);
                            $bayiler = $bayi->getList($staff_info->SIRKET_ID);
                            $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);


                            echo $twig->render('satis-yeni.html', array(
                                'warehouses' => $warehouses,
                                'products' => $products,
                                'bayiler' => $bayiler
                            ));
                        } else {
                            header('Location: ' . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'kontrol':

                        if ($is_login and $role) {


                            $bayi_id = $_POST['BAYI_ID'];
                            $depo_id = $_POST['DEPO_ID'];
                            $urun = $_POST['URUN'];
                            if (!is_numeric($bayi_id)) {
                                echo 'Lütfen geçerli bir bayi seçiniz';
                                die;
                            }

                            if (!is_numeric($depo_id)) {
                                echo 'Lütfen geçerli bir depo seçiniz';
                                die;
                            }

                            $urun_count = count($urun);
                            foreach ($urun as $key => $value) {

                                if ($value['ADET'] > 0) {
                                    $urun_sayisi = $stockCls->checkProductWarehouse($key, $depo_id);
                                    if (!$urun_sayisi) {
                                        $error[] = array('urun_id' => $key, 'depo_id' => $depo_id, 'message' => 'Yetersiz Stok');
                                    }

                                } else {
                                    unset($urun[$key]);
                                }
                            }

                            if ($error) {
                                foreach ($error as $message) {
                                    $urun_info = $stockCls->getProductInfo($message['urun_id']);
                                    $msg .= $urun_info['URUN_ADI'] . '<br>';
                                }


                                $msg .= 'Seçtiğiniz depoda yeterli sayıda ürün bulunmamaktadır.';

                                echo $msg;
                                die;
                            } else {

                                $siparis['BAYI_ID'] = $bayi_id;
                                $siparis['PERSONEL_ID'] = $staff_info->ID;
                                $siparis['DURUM'] = 'beklemede';
                                $siparis['TARIH'] = date('Y-m-d H:i:s');
                                $siparis['DEPO_ID'] = $depo_id;
                                $result = $order->newOrder($siparis, $urun);

                                if ($result) {
                                    error_log($result);
                                    $process_array = array(
                                        'PERSONEL_ID' => $staff_info->ID,
                                        'URUN_ID' => $result['order_id'],
                                        'CIKIS_DEPO_ID' => $depo_id,
                                        'GIRIS_DEPO_ID' => $bayi_id,
                                        'ADET' => $data['ADET'],
                                        'ISLEM_TURU' => 'Siparis'
                                    );
                                    $stockCls->newStockProcess($process_array);
                                    echo 'Siparişiniz tamamlanmıştır.';
                                } else {
                                    echo 'Sipariş eklemede bir problem yaşanmıştır';
                                }
                            }

                        } else {
                            echo 'Yetkisiz Alan';

                        }
                        break;
                    case 'bekleyen':
                        if ($is_login and $role) {

                            $order_list = $order->getOrders('beklemede');
                            foreach ($order_list as $key => $item) {

                                $bayi_info = $bayi->getInfo($item['BAYI_ID']);
                                $order_list[$key]['BAYI_ADI'] = $bayi_info['ADI'];
                                $personal_info = $staff->getStaffInfo($item['PERSONEL_ID']);
                                $order_list[$key]['PERSONEL_ADI'] = $personal_info['ISIM'];
                                unset($bayi_info);
                                unset($personal_info);

                                $order_details = $order->getProductsOfOrders($item['ID']);
                                foreach ($order_details as $key2 => $item2) {
                                    $product_info = $stockCls->getProductInfo($item2['URUN_ID']);
                                    $order_details[$key2]['URUN_ADI'] = $product_info['URUN_ADI'];

                                    unset($product_info);
                                }

                                $order_list[$key]['urunler'] = $order_details;
                            }


                            echo $twig->render('satis-bekleyen.html', array(
                                'bekleyen_siparisler' => $order_list
                            ));
                        } else {
                            header('Location: ' . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'onaylanmis':
                        if ($is_login and $role) {

                            $order_list = $order->getOrders('onaylandi');
                            foreach ($order_list as $key => $item) {

                                $bayi_info = $bayi->getInfo($item['BAYI_ID']);
                                $order_list[$key]['BAYI_ADI'] = $bayi_info['ADI'];
                                $personal_info = $staff->getStaffInfo($item['PERSONEL_ID']);
                                $order_list[$key]['PERSONEL_ADI'] = $personal_info['ISIM'];
                                unset($bayi_info);
                                unset($personal_info);

                                $order_details = $order->getProductsOfOrders($item['ID']);
                                foreach ($order_details as $key2 => $item2) {
                                    $product_info = $stockCls->getProductInfo($item2['URUN_ID']);
                                    $order_details[$key2]['URUN_ADI'] = $product_info['URUN_ADI'];

                                    unset($product_info);
                                }
                                $order_list[$key]['urunler'] = $order_details;
                            }


                            echo $twig->render('satis-onaylanmis.html', array(
                                'onaylanmis_siparisler' => $order_list
                            ));
                        } else {
                            header('Location: ' . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'onay':
                        if ($is_login and $role) {

                            if ($_GET['t'] == 'accept') {
                                $result = $order->setOrderStatus($route->getId(), 'onaylandi');


                                if ($result) {
                                    $order_data = $order->getOrderDetails($route->getId());
                                    foreach ($order_data['products'] as $item) {
                                        $stockCls->dropStock($item['URUN_ID'], $item['ADET']);
                                        $stockCls->dropWarehouseStock($order_data['order_info']['DEPO_ID'], $item['URUN_ID'], $item['ADET']);
                                        $stockCls->addStockToBayi($order_data['order_info']['BAYI_ID'], $item['URUN_ID'], $item['ADET']);
                                    }
                                    $process_array = array(
                                        'PERSONEL_ID' => $staff_info->ID,
                                        'URUN_ID' => $order_data['order_info']['ID'],
                                        'CIKIS_DEPO_ID' => $order_data['order_info']['DEPO_ID'],
                                        'GIRIS_DEPO_ID' => $order_data['order_info']['BAYI_ID'],
                                        'ADET' => 0,
                                        'ISLEM_TURU' => 'Siparis Onay'
                                    );
                                    $stockCls->newStockProcess($process_array);
                                    echo 'İlgili sipariş onaylanmıştır';

                                } else {
                                    echo 'İşlem başarısız';
                                }
                                die;
                            }

                            if ($_GET['t'] == 'reject') {
                                $order->setOrderStatus($route->getId(), 'reddedildi');

                                if ($result) {

                                    echo 'İlgili sipariş onaylanmıştır';
                                } else {
                                    echo 'İşlem başarısız';
                                }
                                die;
                            }

                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    default:
                        header('Location: ' . \stok\Setting::BE_URL);
                        break;
                }
                break;
            // Depo Modülü
            case 'birim':
                switch ($route->getSub()) {
                    case 'ekle':
                        if ($is_login and $role) {
                            $birimler = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                            echo $twig->render('birim-ekle.html', array('birimler' => $birimler));

                        } else {
                            header("Location: " . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'kaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $value) {
                                $data[$key] = removeXSS($value);
                            }
                            $data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            $result = $warehouseCls->newWarehouse($data);
                            if ($result) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız olmuştur';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'duzenle':
                        if ($is_login and $role) {
                            if ($route->getId()) {
                                $birim_data = $warehouseCls->getInfo($route->getId(), $staff_info->SIRKET_ID);
                                $yetkililer = $warehouseCls->yetkiliListesi($route->getId());


                                echo $twig->render('birim-duzenle.html', array(
                                    'yetkililer' => $yetkililer,
                                    'birim' => $birim_data
                                ));
                            } else {
                                header("Location: " . \stok\Setting::BE_URL . '/birim/ekle');
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'guncelle':
                        if ($is_login and $role) {
                            $warehouseCls->update($route->getId(), $_POST);
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'yetkiliekle':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXss($val);
                            }

                            $birim_id = $route->getId();
                            if ($birim_id) {
                                $data['BIRIM_ID'] = $birim_id;
                                $warehouseCls->yetkiliEkle($data);
                            } else {
                                echo 'Geçersiz Birim';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'yetkilisil':
                        if ($is_login and $role) {
                            $warehouseCls->deleteYetkili($route->getId());
                        } else {
                            echo 'Yetkisiz alan';
                        }
                        break;
                    case 'stok':
                        if ($is_login and $role) {
                            if (!$route->getId()) {
                                header("Location: " . \stok\Setting::BE_URL);
                            }
                            $birim_info = $warehouseCls->getInfo($route->getId());
                            $urun_durum = $rapor->mevcutDepoRapor($route->getId());
                            $urun_listesi = $warehouseCls->getStock($route->getId());

                            echo $twig->render('birim-stok.html', array(
                                'urun_listesi' => $urun_listesi,
                                'urun_durum' => $urun_durum,
                                'birim_info' => $birim_info
                            ));
                        } else {
                            header("Location: " . \stok\Setting::BE_URL);
                        }
                        break;
                    default:
                        header("Location: " . \stok\Setting::BE_URL);

                }

                break;
            case 'bayi':
                switch ($route->getSub()) {
                    case 'yeni':
                        if ($is_login and $role) {

                            echo $twig->render('bayi-ekle.html');
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'kaydet':
                        if ($is_login and $role) {
                            $data = $_POST;
                            foreach ($data['BAYI'] as $key => $val) {
                                $data['BAYI'][$key] = removeXSS($val);
                                if (empty($val)) {
                                    echo "$key alanı boş olamaz";
                                    die;
                                }
                            }

                            foreach ($data['YETKILI'] as $key => $val) {
                                $data['YETKILI'][$key] = removeXSS($val);
                                if (empty($val)) {
                                    echo "$key alanı boş olamaz";
                                    die;
                                }
                            }

                            $check = $bayi->isExists($data['BAYI']['ADI']);
                            if ($check) {
                                echo "Bu bayi daha önce eklenmiştir";
                                die;
                            }

                            $data['BAYI']['SIRKET_ID'] = $staff_info->SIRKET_ID;

                            $bayi->bayiNew($data);
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'liste':
                        if ($is_login and $role) {
                            $bayi_listesi = $bayi->getList($staff_info->SIRKET_ID);
                            echo $twig->render('bayi-listesi.html', array('bayi_listesi' => $bayi_listesi));
                        } else {
                            echo 'Yetkisiz Alan';
                        }

                        break;
                    case 'duzenle':
                        if ($is_login and $role) {
                            $bayi_info = $bayi->getInfo($route->getId());
                            $yetkililer = $bayi->yetkiliListesi($route->getId());

                            echo $twig->render('bayi-duzenle.html', array(
                                'yetkililer' => $yetkililer,
                                'bayi_info' => $bayi_info
                            ));
                        } else {
                            header("Location: " . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'guncelle':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }

                            $result = $bayi->bayiUpdate($data, $route->getId());
                            if ($result) {
                                echo 'İşlem başarılı';

                            } else {
                                echo 'İşlem başarısız';
                            }

                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'yetkilikaydet':
                        if ($is_login and $role) {

                            $bayi->yeniyetkili($data);
                        }
                        break;
                    case 'rapor':
                        if ($is_login and $role) {
                            $bayi_info = $bayi->getInfo($route->getId());
                            $orders_count = $order->getOrderByBayi($route->getId(), 'onaylandi');
                            $oran = $rapor->productOranByBayi($route->getId());


                            echo $twig->render('bayi-rapor.html', array(
                                'orders_count' => $orders_count,
                                'oran' => $oran,
                                'bayi_info' => $bayi_info
                            ));
                        } else {
                            header("Location: " . \stok\Setting::BE_URL);
                        }
                        break;
                    default:
                        header("Location: " . \stok\Setting::BE_URL);

                }
                break;
            case 'ayarlar':
                $modLogin = new \modules\modLogin();
                switch ($route->getSub()) {
                    case 'kullanicilistesi':
                        if ($is_login and $role) {
                            $staff_list = $company->getCompanyStaffList($staff_info->SIRKET_ID);

                            echo $twig->render('kullanicilar.html', array(
                                'staff_list' => $staff_list
                            ));
                        } else {
                            header("Location: " . \stok\Setting::BE_URL);
                        }
                        break;

                    case 'kullanicikaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $staff_data[$key] = removeXSS($val);
                            }
                            $staff_data['SIFRE'] = password_hash($staff_data['SIFRE'], PASSWORD_DEFAULT);
                            $staff_data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            $user_check = $staff->getStaffInfo($staff_data['EMAIL']);
                            if ($user_check) {
                                echo 'Bu email adresi ile daha önceden bir kayıt yapılmış';
                            } else {
                                $result = $staff->newStaff($staff_data);
                                if ($result > 0) {
                                    echo 'İşlem Başarılı';
                                } else {
                                    echo 'İşlem Başarısız';
                                }
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'kullanicisil':
                        if ($is_login and $role) {
                            $delete_staff_id = $route->getId();
                            $result = $staff->deleteStaff($delete_staff_id, $staff_info->SIRKET_ID);
                            if ($result > 0) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;

                    case 'kullaniciduzenle':
                        if ($is_login and $role) {
                            $id = $route->getId();

                            $staff_data = $staff->getStaffInfo($id);
                            if ($staff_data['SIRKET_ID'] == $staff_info->SIRKET_ID) {
                                $permission_list = $staff->getPermissionList($id);
                                $module_list = $modules->getModuleList();

                                foreach ($module_list as $module) {
                                    foreach ($permission_list as $perm) {

                                        $find = array_search($module['ID'], $perm);
                                        if ($find) {
                                            $sil = true;
                                        }
                                    }
                                    if (!$sil) {
                                        $no_perms[] = $module;
                                    }
                                    $sil = false;
                                }
                                echo $twig->render('kullanici-duzenle.html', array(
                                    'permission_list' => $permission_list,
                                    'module_list' => $no_perms,
                                    'staff_data' => $staff_data,
                                ));
                            } else {
                                header("Location: " . \stok\Setting::BE_URL);
                            }
                        } else {
                            header("Location: " . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'kullaniciguncelle':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $staff_data[$key] = removeXSS($val);
                            }
                            $staff_id = $staff_data['ID'];

                            $staff_data['SIFRE'] = md5($staff_data['SIFRE']);
                            unset($staff_data['ID']);
                            $result = $staff->updateStaffInfo($staff_id, $staff_data);
                            if ($result) {
                                echo 'İşlem Başarılı olmuştur.';
                            } else {
                                echo 'İşlem Başarısız olmuştur.';
                            }
                        } else {
                            echo 'Yetkisiz İşlem';
                        }
                        break;
                    case 'kullaniciyetkisil':
                        if ($is_login and $role) {
                            $perm_id = $route->getId();
                            $result = $staff->deletePermission($perm_id);
                            if ($result > 0) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız';
                            }
                        } else {
                            echo 'Yetkisiz İşlem';
                        }
                        break;
                    case 'kullaniciyetkiekle':
                        if ($is_login and $role) {
                            $staffId = $route->getId();
                            $perm_id = $_POST['module'];
                            $get_perm = $staff->checkPermission($staffId, $perm_id);
                            if ($get_perm) {
                                echo 'Bu yetki daha önce tanımlanmıştır.';
                            } else {
                                $result = $staff->newPermission($staffId, $perm_id);
                                if ($result > 0) {
                                    echo 'Yetki Eklendi';
                                } else {
                                    echo 'Yetki Ekleme Başarısız Oldu';
                                }
                            }
                        } else {
                            echo 'Yetkisiz İşlem';
                        }
                        break;

                    // Ayarlar->Sirket

                    case 'surecayarlari':
                        if ($is_login and $role) {
                            $company_setting = $company->getCompanySetting($staff_info->SIRKET_ID);
                            $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                            echo $twig->render('surec-ayarlari.html', array(
                                'warehouses' => $warehouses,
                                'company_setting' => $company_setting,
                                'bayi_info' => $bayi_info
                            ));;
                        } else {
                            header('Location:' . \stok\Setting::BE_URL);
                        }
                        break;
                    case 'surecayarkaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $setting[$key] = removeXSS($val);
                            }
                            $result = $company->updateCompanySetting($staff_info->SIRKET_ID, $setting);
                            if ($result) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;

                    default:
                        header('Location: ' . \stok\Setting::BE_URL);
                        break;
                }
                break;
            default:
                if (!$is_login) {
                    echo $twig->render('login.html', array(
                        'message' => $_GET['msg']
                    ));
                    echo $html;
                } else {

                    $aktif_sirket_id = $_SESSION['sirket_id'];
                    $urun_stok_durumlari = $rapor->acceptedOrdersByProduct();

                    $depolar = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                    foreach ($depolar as $item) {
                        $report[$item['ID']] = $rapor->mevcutDepoRapor($item[ID]);
                        $report[$item['ID']]['DEPO_ADI'] = $item['BIRIM_ADI'];
                    }

                    $gunluk_oran = $rapor->gunlukOnaylanmisSatisOranlari();

                    echo $twig->render('index.html', array(
                        'grafik_data' => $report,
                        'gimat_stok' => $gimat_stok,
                        'fabrika_stok' => $fabrika_stok,
                        'urun_stok_durum' => $urun_stok_durumlari
                    ));
                }

                break;
        }
        break;

    default:
        echo $twig->render('error.html');
        break;
}
