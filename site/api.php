<?php
/**
 * Created by PhpStorm.
 * User: ozgur.kuru
 * Date: 20.5.2015
 * Time: 16:08
 */


session_set_cookie_params(60000, "/", "fabrika.stokizle.com");
session_start();
require_once('/srv/stokizle.com.fabrika/site/config.php');
require_once(stok\Setting::FW_PATH . "lib/general.lib.php");
require_once(stok\Setting::FW_PATH . 'lib/rain.tpl.class.php');
require_once(stok\Setting::FW_PATH . "lib/memcache.lib.php");
require_once(stok\Setting::FW_PATH . "lib/mysql.lib.php");
require_once(stok\Setting::FW_PATH . "lib/routing.lib.php");
require_once(stok\Setting::FW_PATH . "lib/phpmailer/class.phpmailer.php");
require_once(stok\Setting::FW_PATH . "lib/sms.lib.php");
require_once(stok\Setting::FW_PATH . "modules/routing.mod.php");
require_once(stok\Setting::FW_PATH . "modules/db.mod.php");

require_once(stok\Setting::SITE_PATH . "class/mail.php");

require_once(stok\Setting::SITE_PATH . "modules/login.mod.php");

// yeni sistem
require_once(stok\Setting::SITE_PATH . "class/staff.php");
require_once(stok\Setting::SITE_PATH . "class/modules.php");
require_once(stok\Setting::SITE_PATH . "class/company.php");
require_once(stok\Setting::SITE_PATH . "class/stock.php");
require_once(stok\Setting::SITE_PATH . "class/warehouse.php");
require_once(stok\Setting::SITE_PATH . "class/reports.php");
require_once(stok\Setting::SITE_PATH . "class/bayi.php");


$routing = new arte\Routing();
$route = new arte\Routes($routing->router());
$debug = $route->getDebug();

$database = new arte\Database(array(
    'user' => stok\Setting::MYSQL_USER,
    'password' => stok\Setting::MYSQL_PASSWORD,
    'host' => stok\Setting::MYSQL_HOST,
    'database' => stok\Setting::MYSQL_DATABASE
));

$db = new arte\db();

$mailer = new PHPMailer();
$mail = new stok\Mail();

$stockCls = new stok\Stock();

// yeni sistem
$modules = new stok\Modules();
$staff = new stok\Staff();
$company = new stok\Company();
$warehouseCls = new stok\warehouse();
$sms = new arte\sms();
$rapor = new stok\reports();
$bayi = new stok\bayi();




switch($route->getSub()){
    case 'bayiSec':

        break;
}