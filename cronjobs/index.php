<?php

session_set_cookie_params(60000, "/", "stokizle.com");
session_start();
require_once ('/srv/stokizle.com/site/config.php');
require_once (stok\Setting::FW_PATH . "lib/general.lib.php");
require_once (stok\Setting::FW_PATH . 'lib/rain.tpl.class.php');
require_once (stok\Setting::FW_PATH . "lib/memcache.lib.php");
require_once (stok\Setting::FW_PATH . "lib/mysql.lib.php");
require_once (stok\Setting::FW_PATH . "lib/routing.lib.php");
require_once (stok\Setting::FW_PATH . "lib/phpmailer/class.phpmailer.php");
require_once (stok\Setting::FW_PATH . "lib/sms.lib.php");
require_once (stok\Setting::FW_PATH . "modules/routing.mod.php");
require_once (stok\Setting::FW_PATH . "modules/db.mod.php");

require_once (stok\Setting::SITE_PATH . "class/payment.php");
require_once (stok\Setting::SITE_PATH . "class/mail.php");

require_once (stok\Setting::SITE_PATH . "modules/login.mod.php");

// yeni sistem
require_once (stok\Setting::SITE_PATH . "class/staff.php");
require_once (stok\Setting::SITE_PATH . "class/modules.php");
require_once (stok\Setting::SITE_PATH . "class/company.php");
require_once (stok\Setting::SITE_PATH . "class/stock.php");
require_once (stok\Setting::SITE_PATH . "class/warehouse.php");
require_once (stok\Setting::SITE_PATH . "class/reports.php");
// onemli tanimlar

$routing = new arte\Routing();
$route = new arte\Routes($routing->router());
$debug = $route->getDebug();
$database = new arte\Database(array(
    'user' => stok\Setting::MYSQL_USER,
    'password' => stok\Setting::MYSQL_PASSWORD,
    'host' => stok\Setting::MYSQL_HOST,
    'database' => stok\Setting::MYSQL_DATABASE
    ));

$db = new arte\db();

$mailer = new PHPMailer();
$mail = new stok\Mail();

$stockCls = new stok\Stock();
$payment = new stok\Payment();

// yeni sistem
$modules = new stok\Modules();
$staff = new stok\Staff();
$company = new stok\Company();
$warehouseCls = new stok\warehouse();
$sms = new arte\sms();
$rapor = new stok\reports();
switch ($route->getSubdomain()) {
    case 'www':

        // if ($_SERVER['HTTPS'] != "on") {
        // header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        // }
        raintpl::configure("base_url", stok\Setting::SITE_URL);
        raintpl::configure("tpl_dir", stok\Setting::SITE_TPL_PATH . '/');
        raintpl::configure("cache_dir", "/tmp/");
        raintpl::configure('path_replace', false);

        $tpl = new RainTPL();
        $tpl->assign('tpl_url', stok\Setting::SITE_TPL_URL);
        $tpl->assign('title', stok\Setting::TITLE);
        switch ($route->getMaster()) {

            case 'sistemdurumu':
                $html = $tpl->draw('sistemdurumu', $return_string = true);
                echo $html;
                break;
            case 'iletisim':
                $html = $tpl->draw('iletisim', $return_string = true);
                echo $html;
                break;
            case 'kullanicisozlesmesi':
                $tpl->assign('sozlesme_baslik', 'Kullanıcı Sözleşmesi');
                $tpl->assign('sozlesme', 'k');
                $html = $tpl->draw('sozlesme', $return_string = true);
                echo $html;
                break;
            case 'gizliliksozlesmesi':
                $tpl->assign('sozlesme_baslik', 'Gizlilik Sözleşmesi');
                $tpl->assign('sozlesme', 'gizlilik');
                $html = $tpl->draw('sozlesme', $return_string = true);
                echo $html;
                break;
            case 'mesafelisatis':
                $tpl->assign('sozlesme_baslik', 'Mesafeli Satış Sözleşmesi');
                $tpl->assign('sozlesme', 'mesafeli');
                $html = $tpl->draw('sozlesme', $return_string = true);
                echo $html;
                break;

            default:
                $html = $tpl->draw('index', $return_string = true);
                echo $html;
                break;
        }
        break;

    case 'panel':
        if ($_SERVER['HTTPS'] != "on") {
            header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        }
        $time = 3;
        $is_login = $staff->isLogin();
        $staff_info = json_decode($_SESSION['info']);
        $staff_profile = json_decode($_SESSION['profile']);

        raintpl::configure("base_url", stok\Setting::BE_URL);
        raintpl::configure("tpl_dir", stok\Setting::BE_TPL_PATH . '/');

        raintpl::configure("cache_dir", stok\Setting::CACHE_PATH . 'backend/');
        raintpl::configure('path_replace', false);
        $tpl = new RainTPL();
        $tpl->assign('tpl_url', stok\Setting::BE_TPL_URL);
        $tpl->assign("profile_pic_dir", stok\Setting::USER_FILES_URL . '/');
        $tpl->assign('title', 'Stok Yönetim Paneli');

        if ($is_login) {
            $tpl->assign('staff_info', $staff_info);
            $tpl->assign('last_login', $_SESSION['last_login']);
            $tpl->assign('user_screen_name', $staff_info->ISIM);
            $tpl->assign('sirket_data', $_SESSION['sirket_data']);
            $tpl->assign('id', $staff_info->ID);
            if ($_SESSION['sirket_id']) {
                $tpl->assign('sirket_id', $_SESSION['sirket_id']);
                $sirket_id = $_SESSION['sirket_id'];
                $obj_key = 'id' . $sirket_id;
                $user_menu = $modules->getMenu($staff_info->ID);
                $role = $staff->roleFind($user_menu, '/' . $route->getMaster() . '/' . $route->getSub());

                $tpl->assign('user_menu', $user_menu);
            }
            if($_SESSION['sirket_data']['DURUM']=='odeme'){
                echo $tpl->draw('satinal',true);
                die;
            }

        }
        switch ($route->getMaster()) {
            case 'api':
                switch ($route->getSub()) {
                    case 'mailCheck':
                        $email = removeXSS($_GET['EMAIL']);
                        $result = json_encode($staff->checkEmail($email));
                        echo $result;
                        break;
                    case 'phoneCheck':
                        $phone = removeXSS($_GET['TELEFON']);
                        $result = json_encode($staff->checkPhone($phone));
                        echo $result;
                        break;
                    case 'smsValidate':
                        foreach ($_POST as $key => $val) {
                            $data[$key] = removeXSS($val);
                        }
                        $result = $staff->checkSmsValidation($data['TELEFON'], $data['EMAIL'], $data['SMS_KODU']);
                        if ($result) {
                            $staff->deleteSmsValidation($data['TELEFON'], $data['EMAIL']);
                            $staff_data['ISIM'] = $data['ISIM'];
                            $staff_data['EMAIL'] = $data['EMAIL'];
                            $staff_data['SIFRE'] = md5($data['SIFRE']);
                            $staff_data['SIRKET_ID'] = 0;
                            $result = $staff->newStaff($staff_data);

                            if ($result) {
                                $staff_profile['KULLANICI_ID'] = $result['ID'];
                                $staff_profile['TELEFON'] = $data['TELEFON'];
                                $result = $staff->newStaffProfile($staff_profile);
                                if ($result) {
                                    echo 'Kaydınız yapılmıştır';
                                } else {
                                    echo 'Kaydınız yapılamamıştır. Lütfen Tekrar Deneyiniz';
                                }
                            } else {
                                echo 'Kaydınız yapılamamıştır. Lütfen Tekrar Deneyiniz';
                            }
                        } else {
                            echo 'SMS Doğrulama Başarısız.';
                        }
                        break;
                    default:
                        echo 'no entry!';
                        break;
                }
                break;
            case 'odemeonay':
                $token = $_GET['token'];
                $paket = $_GET['p'];
                $paket_bilgisi = stok\Setting::$PAKET[$paket];
                $result = $payment->getResult($token, $_SESSION['sirket_id'], $staff_info->id);
                if ($result['stat'] == "success") {
                    $paket_bitis = addDate($paket_bilgisi['period']);
                    $update_data = array(
                        'paket_bitis_tarihi' => $paket_bitis,
                        'paket' => $paket
                    );
                    $customerCls->updateCompanyInfo($_SESSION['sirket_id'], $update_data);
                }
                $tpl->assign('message', $result['message']);
                $tpl->assign('time', '5');
                $tpl->assign('url', stok\Setting::BE_URL);
                $html = $tpl->draw('notice', $return_string = true);
                echo $html;
                break;

            case 'kaydet':
                switch ($route->getSub()) {
                    case 'sirket':
                        foreach ($_POST as $key => $value) {
                            $data[$key] = removeXSS($value);
                        }
                        $data['yetkili_kullanici_id'] = $staff_info->id;
                        $data['paket'] = '15deneme';
                        $data['durum'] = 'aktif';
                        $data['paket_bitis_tarihi'] = addDate(15);
                        $result = $customerCls->newCompany($data);
                        if ($result > 0) {
                            $insert_user = $customerCls->addUserToCompany(array(
                                'kullanici_id' => $staff_info->id,
                                'sirket_id' => $result,
                                'yetki' => 'root'
                            ));
                            $_SESSION['sirket_id'] = $result;
                            $_SESSION['info'] = json_encode($customerCls->getUserInfo($_SESSION['user_email']));
                            $tpl->assign('message', 'Kayıt Başarılı. Yönlendiriliyorsunuz...');
                            $tpl->assign('time', '5');
                            $tpl->assign('url', stok\Setting::BE_URL);
                            $html = $tpl->draw('notice', $return_string = true);
                        }
                        break;

                    default:
                        $tpl->assign('message', 'Yanlış bir yerdesiniz...');
                        $tpl->assign('time', '3');
                        $tpl->assign('url', stok\Setting::BE_URL);
                        $html = $tpl->draw('notice', $return_string = true);
                        break;
                }
                echo $html;
                break;

            case 'paketislemleri':
                if ($is_login and $role) {
                    $sirket_bilgileri = $customerCls->getCompanyInfo($_SESSION['sirket_id']);
                    $paket_bilgileri = stok\Setting::$PAKET[$sirket_bilgileri['paket']];
                    $tpl->assign('sirket_bilgileri', $sirket_bilgileri);
                    $tpl->assign('paket_bilgileri', $paket_bilgileri);
                    $html = $tpl->draw('paketislemleri', $return_string = true);
                } else {
                    $tpl->assign('message', 'Bu alana giriş yapmak için yetkiniz bulunmamaktadır...');
                    $tpl->assign('time', '3');
                    $tpl->assign('url', stok\Setting::BE_URL);
                    $html = $tpl->draw('notice', $return_string = true);
                }
                echo $html;
                break;
            case 'satinal':
                if ($is_login) {
                    $paket = stok\Setting::$PAKET[$_GET['p']];
                    $isim = explode(" ", $staff_info->isim);

                    if (count($isim) > 2) {
                        $ad = $isim[0] . " " . $isim[1];
                        $soyad = $isim[2];
                    } else {
                        $ad = $isim[0];
                        $soyad = $isim[1];
                    }

                    $sirket_bilgileri = $customerCls->getCompanyInfo($_SESSION['sirket_id']);
                    $tpl->assign('SCREENNAME', $staff_info->isim);
                    $adres = $sirket_bilgileri['adres'] . ' ' . $sirket_bilgileri['sehir'];
                    $tpl->assign('ADRES', $adres);
                    $tpl->assign('TELEFON', $sirket_bilgileri['telefon']);
                    $tpl->assign('paket_bilgisi', $paket);
                    $date = date("d-m-Y H:i:s");
                    $tpl->assign('tarih', $date);
                    if ($_GET['o'] == 'onaylandi') {
                        $url = "https://ctpe.net/frontend/GenerateToken";
                        $data = "SECURITY.SENDER=8a8394c743706c040143aff6c00e0d2e" . "&TRANSACTION.CHANNEL=8a8394c343706c170143aff6c1f9192c" . "&TRANSACTION.MODE=LIVE" . "&USER.LOGIN=8a8394c743706c040143aff6c04d0d32" . "&USER.PWD=M7hsf7ZTBr" . "&PAYMENT.TYPE=DB" . "&IDENTIFICATION.TRANSACTIONID=1" . "&PRESENTATION.USAGE=stokizle.com" . "&PRESENTATION.AMOUNT=" . $paket['price'] . "&PRESENTATION.CURRENCY=TRY" . "&NAME.GIVEN=" . $ad . "&NAME.FAMILY=" . $soyad . "&NAME.COMPANY=" . $sirket_bilgileri['isim'] . "&ADDRESS.STREET=" . $sirket_bilgileri['adres'] . "&ADDRESS.ZIP=0" . "&ADDRESS.CITY=" . $sirket_bilgileri['sehir'] . "&ADDRESS.STATE=TR" . "&ADDRESS.COUNTRY=TR" . "&CONTACT.PHONE=" . $sirket_bilgileri['telefon'] . "&CONTACT.EMAIL=" . $staff_info->email . "&CONTACT.IP=" . $_SERVER['REMOTE_ADDR'];

                        $payment->requestToken($url, $data);
                        $token = $payment->getToken();
                        $tpl->assign('token', $token);

                        $js_files[] = 'https://ctpe.net/frontend/widget/v2/widget.js?language=tr&amp;style=card';
                        $tpl->assign('js_import', '1');
                        $tpl->assign('js_files', $js_files);
                        $tpl->assign('payment', 'evet');
                    }
                    echo $tpl->draw('satinal', true);
                } else {
                    $tpl->assign('message', 'Bu alana giriş yapmak için yetkiniz bulunmamaktadır...');
                    $tpl->assign('time', '3');
                    $tpl->assign('url', stok\Setting::BE_URL . '/paketislemleri');
                    $html = $tpl->draw('notice', $return_string = true);
                }

                break;

            // /----- V1.0
            // Genel Modüller
            case 'yenisirket':
                if ($is_login) {
                    foreach ($_POST as $key => $val) {
                        $company_data[$key] = removeXSS($val);
                    }
                    $result = $company->newCompany($company_data, $staff_info->ID);
                    if ($result) {
                        $permission_list = $company->getPermissions();
                        foreach ($permission_list as $permission) {
                            $staff->newPermission($staff_info->ID, $permission['ID']);
                        }
                        echo 'Şirket Eklendi.';
                    } else {
                        echo 'Şirket Ekleme Başarısız. Tekrar deneyin.';
                    }
                } else {
                    echo 'Yetkisiz Alan';
                }
                break;
            case 'kayit':
                if (!$is_login) {
                    if ($_POST) {
                        foreach ($_POST as $key => $val) {
                            $reg_data[$key] = removeXSS($val);
                        }
                        $tpl->assign('reg_data', $reg_data);
                        $sms_data['EMAIL'] = $reg_data['EMAIL'];
                        $sms_data['TELEFON'] = $reg_data['TELEFON'];

                        $staff->smsValidate(stok\Setting::$SMS, $sms_data);
                        $html = $tpl->draw('company-register-check', true);
                    } else {
                        $html = $tpl->draw('company-register', $return_string = true);
                    }
                } else {
                    header('Location: https://panel.stokizle.com');
                }
                echo $html;
                break;

            case 'giris':
                $modLogin = new \modules\modLogin();
                $modLogin->login();
                unset($modLogin);
                break;
            case 'cikis':
                $modLogin = new \modules\modLogin();
                $modLogin->logout();
                header('Location:' . stok\Setting::BE_URL);
                break;
            case 'sifremiunuttum':
            case 'sifreyenile':
                $id = $route->getId();
                if ($id > 0) {

                    $tpl->assign('time', '3');

                    $email = removeXSS($_POST['login']);
                    $token = generateRandomString(25);
                    $result = $staff->passwordReset($email, $token);
                    if ($result > 0) {

                        $to['{LOGINNAME}'] = $email;
                        $to['{TOKEN}'] = $token;
                        $mail->send("noreply", "Şifre Hatırlatma", $to, 'sifreyenileme');
                    } else {
                        header('Location:' . stok\Setting::BE_URL . '/?msg=Şifre yenilemede hata oluşmuştur');
                    }
                    header('Location:' . stok\Setting::BE_URL . '/?msg=Şifreniz yenileneme linki mail adresinize gönderilmiştir');
                } else {
                    $html = $tpl->draw('lostpassword', $return_string = true);
                    echo $html;
                }
                break;
            case 'sifresifirla':
                $token_control = $staff->checkToken($route->getSub());
                if ($token_control['ID'] > 0) {
                    if ($route->getId() > 0) {
                        $data['SIFRE'] = md5(removeXSS($_POST['SIFRE']));
                        $result = $staff->updateStaffInfo($_POST['EMAIL'], $data);

                        if ($result > 0) {
                            $staff->updateTokens($_POST['EMAIL']);
                            header('Location:' . stok\Setting::BE_URL . '/?msg=Şifreniz sıfırlanmıştır. Giriş yapabilirisiniz.');
                        }
                    } else {
                        $tpl->assign('token', $route->getSub());
                        $tpl->assign('email', $token_control['KULLANICI_EMAIL']);
                        $html = $tpl->draw('newpassword', $return_string = true);
                        echo $html;
                    }
                } else {
                    header('Location:' . stok\Setting::BE_URL . '/?msg=Geçersiz TOKEN');
                }
                break;

            // Ürün Modülü
            case 'urun':
                switch ($route->getSub()) {
                    case 'alanlar':
                        if ($is_login and $role) {
                            $product_group_id = $route->getId();
                            if ($product_group_id < 2) {
                                $fields = $stockCls->getAllFields($staff_info->SIRKET_ID);
                                $tpl->assign('fields', $fields);
                                $html = $tpl->draw('stock-fields', $return_string = true);
                            } else {

                                $html = $tpl->draw('stock-groups', $return_string = true);
                            }
                            echo $html;
                        } else {
                            header('Location:' . stok\Setting::BE_URL);
                        }
                        break;
                    case 'alanekle':
                        if ($is_login and $role) {
                            $product_groups = $stockCls->getProductGroups($staff_info->SIRKET_ID);
                            if (!$product_groups) {
                                $disable = true;
                                $msg = '<h2>Ürün alanı ekleyebilmeniz için önce ürün grubu eklemelisiniz...</h2>';
                                $tpl->assign('msg', $msg);
                                $html = $tpl->draw('stock-group-add', true);
                            } else {
                                $tpl->assign('product_groups', $product_groups);
                                $html = $tpl->draw('stock-field-add', true);
                            }
                            echo $html;
                        } else {
                            header('Location:' . stok\Setting::BE_URL);
                        }
                        break;
                    case 'alankaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }
                            $data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            $result = $stockCls->newField($data);
                            if ($result) {
                                echo "Alan Eklendi";
                            } else {
                                echo "Alan Ekleme İşlemi Başarısız";
                            }
                        } else {
                            echo "Yetkisiz Alan";
                        }
                        break;
                    case 'grup':
                        if ($is_login and $role) {
                            $id = $route->getId();
                            if ($id > 0) {
                                $group_info = $stockCls->getGroupInfo($id);
                                $fields = $stockCls->getFields($group_info['STOK_KODU'], $staff_info->SIRKET_ID);
                                $tpl->assign('group_info', $group_info);
                                $tpl->assign('fields', $fields);
                                $html = $tpl->draw('stock-group', $return_string = true);
                            } else {
                                header('Location:' . stok\Setting::BE_URL . '/urun/kodlar');
                            }
                            echo $html;
                        } else {
                            header('Location:' . stok\Setting::BE_URL);
                        }
                        break;
                        break;
                    case 'grupekle':
                        if ($is_login and $role) {
                            $html = $tpl->draw('stock-group-add', true);
                            echo $html;
                        } else {
                            header('Location:' . stok\Setting::BE_URL);
                        }
                        break;
                    case 'grupkaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }
                            $data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            $group_exists = $stockCls->groupExists($data['STOK_KODU'], $data['SIRKET_ID']);
                            if ($group_exists) {
                                echo 'Bu grup daha önce eklenmiştir.';
                            } else {

                                $result = $stockCls->newGroup($data);
                                if ($result) {
                                    echo "Grup Eklendi";
                                } else {
                                    echo "Grup Ekleme İşlemi Başarısız";
                                }
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'kodlar':
                        if ($is_login and $role) {
                            $product_groups = $stockCls->getProductGroups($staff_info->SIRKET_ID);
                            $tpl->assign('product_groups', $product_groups);
                            $html = $tpl->draw('stock-groups', $return_string = true);

                            echo $html;
                        } else {
                            header('Location:' . stok\Setting::BE_URL);
                        }
                        break;
                    default:
                        header('Location:' . stok\Setting::BE_URL);
                        break;
                }

                break;

            //Rapor Modülü
            case 'rapor':
                switch ($route->getSub()) {
                    case 'depo':

                        break;
                    case 'urun':
                        if ($_POST) {
                            foreach ($_POST as $key => $val) {
                                $report_data[$key] = removeXSS($val);
                            }
                            $report_data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            if ($report_data['ISLEM'] == 'satis') {
                                $report_result = $rapor->productSellReport($report_data);
                            }
                            if ($report_data['ISLEM'] == 'giris') {
                                $report_result = $rapor->productAddReport($report_data);
                            }
                            $tpl->assign('report_result', $report_result);
                        }

                        echo $tpl->draw('report-product', true);
                        break;
                    default:
                        $total_product_report = $rapor->getAllStockCount($staff_info->SIRKET_ID);

                        $tpl->assign('product_list', $total_product_report);
                        echo $tpl->draw('report-general', true);
                        break;
                }

                break;
            // Stok Modülü
            case 'stok':
                switch ($route->getSub()) {
                    case 'ekle':
                        if ($is_login and $role) {
                            $grup_check = $stockCls->checkUrunGrup($staff_info->SIRKET_ID);
                            $depo_check = count($warehouseCls->getWarehouses($staff_info->SIRKET_ID));
                            if ($depo_check > 0) {
                                if ($grup_check['ID'] > 0) {
                                    $custom_field_check = $stockCls->checkInfoField($staff_info->SIRKET_ID);
                                    $product_groups = $stockCls->getProductGroups($staff_info->SIRKET_ID);
                                    $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                                    if ($custom_field_check) {
                                        $tpl->assign('warehouses', $warehouses);
                                        $tpl->assign('product_groups', $product_groups);
                                        $tpl->assign('msg', $_SESSION['msg']);
                                        $html = $tpl->draw('stock-add', true);
                                    } else {
                                        $tpl->assign('msg', '<p><b>Hiç bilgi alanı bulunmuyor. Lütfen bilgi alanı ekleyin</b></p>');

                                        $tpl->assign('product_groups', $product_groups);
                                        $html = $tpl->draw('stock-field-add', true);
                                    }
                                } else {
                                    $tpl->assign('msg', '<p><b>Hiç ürün grubunuz bulunmuyor. Lütfen öœrün Grubu Ekleyin</b></p>');
                                    $html = $tpl->draw('stock-group-add', true);
                                }
                            } else {
                                $tpl->assign('msg', '<p><b>Hiç deponuz bulunmuyor. Lütfen Depo Ekleyin</b></p>');
                                $html = $tpl->draw('warehouse-add', true);
                            }
                            echo $html;
                        } else {
                            header('Location: https://panel.stokizle.com');
                        }
                        break;
                    case 'transfer':
                        if ($is_login and $role) {
                            $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                            $tpl->assign('warehouses', $warehouses);
                            $html = $tpl->draw('stock-transfer', true);
                            echo $html;
                        } else {
                            header('Location: https://panel.stokizle.com');
                        }
                        break;
                    case 'transferkaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }
                            $check_stock = $warehouseCls->checkStock($data['CIKIS_DEPO'], $data['BARKOD']);
                            if ($data['CIKIS_DEPO'] == $data['GIRIS_DEPO']) {
                                echo 'Çıkış depo ile giriş depo aynı olamaz';
                            } elseif ($check_stock['ADET'] < $data['ADET']) {
                                echo 'Çıkış depo stok miktarı yeterli değil. Mevcut Stok: ' . $check_stock['ADET'];
                            } else {
                                $transfer_stock['TRANSFER_TARIHI'] = $data['TRANSFER_TARIHI'];
                                $transfer_stock['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                $transfer_stock['PERSONEL_ID'] = $staff_info->ID;
                                $transfer_stock['URUN_ID'] = $check_stock['URUN_ID'];
                                $transfer_stock['CIKIS_DEPO'] = $check_stock['DEPO_ID'];
                                $transfer_stock['GIRIS_DEPO'] = $data['GIRIS_DEPO'];
                                $transfer_stock['ADET'] = $data['ADET'];
                                $transfer_result = $stockCls->stockTransfer($transfer_stock);
                                if ($transfer_result) {
                                    $count = $check_stock['ADET'] - $data['ADET'];
                                    $warehouseCls->updateStock($check_stock['DEPO_ID'], $check_stock['URUN_ID'], $count);

                                    $enter_check = $warehouseCls->checkStock($data['GIRIS_DEPO'], $data['BARKOD']);

                                    if ($enter_check['ID'] > 0) {
                                        $enter_count = $enter_check['ADET'] + $data['ADET'];
                                        $warehouseCls->updateStock($enter_check['DEPO_ID'], $check_stock['URUN_ID'], $enter_count);
                                    } else {
                                        $new_product['DEPO_ID'] = $data['GIRIS_DEPO'];
                                        $new_product['URUN_ID'] = $check_stock['URUN_ID'];
                                        $new_product['ADET'] = $data['ADET'];
                                        $new_product['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                        $stockCls->addProductToWarehouse($new_product);
                                    }
                                    echo 'İşlem Başarılı';
                                } else {
                                    echo 'İşlem başarısız';
                                }
                            }
                        } else {
                            header('Location: https://panel.stokizle.com');
                        }
                        break;
                    case 'satis':
                        if ($is_login and $role) {
                            $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                            $tpl->assign('warehouses', $warehouses);
                            $html = $tpl->draw('stock-sold', true);
                            echo $html;
                        } else {
                            header('Location: https://panel.stokizle.com');
                        }
                        break;

                    case 'satiskaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $data[$key] = removeXSS($val);
                            }
                            $check_stock = $warehouseCls->checkStock($data['DEPO_ID'], $data['BARKOD']);

                            if ($check_stock['ADET'] < 1 OR $check_stock['ADET'] < $data['ADET']) {
                                echo 'Seçilen Depoda Yetersizdir. Stok Adedi: ' . $check_stock['ADET'];
                            } else {
                                $sell_stock['CIKIS_TARIHI'] = $data['CIKIS_TARIHI'];
                                $sell_stock['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                $sell_stock['PERSONEL_ID'] = $staff_info->ID;
                                $sell_stock['URUN_ID'] = $check_stock['URUN_ID'];
                                $sell_stock['CIKIS_DEPO'] = $check_stock['DEPO_ID'];
                                $sell_stock['ADET'] = $data['ADET'];

                                $sell_result = $stockCls->stockSell($sell_stock);

                                if ($sell_result) {
                                    $count = $check_stock['ADET'] - $data['ADET'];
                                    $warehouseCls->updateStock($check_stock['DEPO_ID'], $check_stock['URUN_ID'], $count);
                                    echo 'İşlem başarılı';
                                } else {
                                    echo 'İşlem Başarısız';
                                }
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'kaydet':
                        if ($is_login and $role) {

                            if ($_POST['yeni_urun']) {
                                foreach ($_POST['urun_kimlik'] as $key => $val) {
                                    $product_info[$key] = removeXSS($val);
                                }

                                $product_info['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                $product_result = $stockCls->newProduct($product_info);
                                if ($product_result) {
                                    foreach ($_POST['alan_bilgi'] as $key => $val) {
                                        $stockCls->productInfoField(array(
                                            'ALAN_ID' => $key,
                                            'DEGER' => removeXSS($val),
                                            'BARKOD' => $product_info['BARKOD'],
                                            'SIRKET_ID' => $staff_info->SIRKET_ID
                                        ));
                                    }
                                    $warehouse_data['DEPO_ID'] = $_SESSION['product_add']['warehouse'];
                                    $warehouse_data['ADET'] = $_SESSION['product_add']['count'];
                                    $warehouse_data['URUN_ID'] = $product_result;
                                    $warehouse_data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                    $stockCls->addProductToWarehouse($warehouse_data);
                                    $stok_data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                    $stok_data['PERSONEL_ID'] = $staff_info->ID;
                                    $stok_data['URUN_ID'] = $product_result;
                                    $stok_data['DEPO_ID'] = $warehouse_data['DEPO_ID'];
                                    $stok_data['ADET'] = $warehouse_data['ADET'];

                                    $stockCls->addStock($stok_data);
                                    $_SESSION['msg'] = 'Stok Eklendi';
                                    header('Location: https://panel.stokizle.com/stok/ekle');
                                } else {
                                    echo 'İşlem Başarısız';
                                }
                            } else {

                                foreach ($_POST as $key => $val) {
                                    $data[$key] = removeXSS($val);
                                }
                                $data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                                $product_exists = $stockCls->checkProductExists($data['BARKOD'], $staff_info->SIRKET_ID);
                                if ($product_exists) {
                                    $new_product_data['DEPO_ID'] = $_POST['DEPO_ID'];
                                    $new_product_data['URUN_ID'] = $product_exists['ID'];
                                    $check_warehouse = $stockCls->checkProductWarehouse($product_exists['ID'], $_POST['DEPO_ID']);
                                    if ($check_warehouse) {
                                        $new_product_data['ADET'] = $check_warehouse['ADET'] + $_POST['ADET'];
                                        $result = $stockCls->updateWarehouseStock($new_product_data);
                                    } else {
                                        $new_product_data['ADET'] = $_POST['ADET'];
                                        $result = $stockCls->addProductToWarehouse($new_product_data);
                                    }
                                    if ($result) {
                                        $_SESSION['msg'] = 'Stok Eklenmiştir';
                                    } else {
                                        $_SESSION['msg'] = 'Stok Ekleme Başarısız Olmuştur';
                                    }

                                    header("Location: https://panel.stokizle.com/stok/ekle");
                                    exit();
                                } else {
                                    $tpl->assign('msg', '<p><b>Yeni bir ürün(barkod) girdiniz. Lütfen ürün bilgilerini giriniz...</b></p>');
                                    $product_groups = $stockCls->getProductGroups($staff_info->SIRKET_ID);
                                    $_SESSION['product_add']['count'] = $_POST['ADET'];
                                    $_SESSION['product_add']['warehouse'] = $_POST['DEPO_ID'];

                                    $tpl->assign('product_groups', $product_groups);
                                    $stok_kodu = $_POST['STOK_KODU'];
                                    $tpl->assign('stok_kodu', $stok_kodu);
                                    $tpl->assign('barkod', $_POST['BARKOD']);

                                    $fields = $stockCls->getFields($data['STOK_KODU'], $staff_info->SIRKET_ID);

                                    $tpl->assign('fields', $fields);
                                    $html = $tpl->draw('stock-info-add', true);
                                    echo $html;
                                }
                            }
                        } else {
                            header("Location: https://panel.stokizle.com");
                        }
                        break;
                    default:
                        header('Location:' . stok\Setting::BE_URL);
                        break;
                }
                break;
            // Depo Modülü
            case 'depo':
                switch ($route->getSub()) {
                    case 'ekle':
                        if ($is_login and $role) {
                            $html = $tpl->draw('warehouse-add', true);
                            echo $html;
                        } else {
                            header("Location: https://panel.stokizle.com");
                        }
                        break;
                    case 'kaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $value) {
                                $data[$key] = removeXSS($value);
                            }
                            $data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            $result = $warehouseCls->newWarehouse($data);
                            if ($result) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız olmuştur';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'liste':
                        if ($is_login and $role) {
                            $warehouse_list = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                            $tpl->assign('warehouse_list', $warehouse_list);
                            $html = $tpl->draw('warehouse-list', true);
                            echo $html;
                        }
                        break;
                    default:
                        if ($route->getId()) {
                            $warehouse_info = $warehouseCls->getInfo($route->getId(), $staff_info->SIRKET_ID);
                            if ($warehouse_info) {
                                $warehouse_stock = $warehouseCls->getStock($route->getId());
                                $tpl->assign('warehouse_info', $warehouse_info);
                                $tpl->assign('warehouse_stock', $warehouse_stock);
                                $html = $tpl->draw('warehouse-profile', true);
                                echo $html;
                            } else {
                                header("Location: https://panel.stokizle.com");
                            }
                        } else {
                            header("Location: https://panel.stokizle.com");
                        }
                }

                break;
            // Personel Modülü

            case 'personel':
                switch ($route->getSub()) {
                    case 'liste':
                        $staff_list = $company->getCompanyStaffList($staff_info->SIRKET_ID);
                        $tpl->assign('staff_list', $staff_list);
                        $html = $tpl->draw('staff-list', $return_string = true);
                        echo $html;
                        break;
                    case 'ekle':
                        $html = $tpl->draw('staff-add', $return_string = true);
                        echo $html;
                        break;
                    case 'sil':
                        if ($is_login and $role) {
                            $delete_staff_id = $route->getId();
                            $result = $staff->deleteStaff($delete_staff_id);
                            if ($result > 0) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'kaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $staff_data[$key] = removeXSS($val);
                            }
                            $staff_data['SIFRE'] = md5($staff_data['SIFRE']);
                            $staff_data['SIRKET_ID'] = $staff_info->SIRKET_ID;
                            $user_check = $staff->getStaffInfo($staff_data['EMAIL']);
                            if ($user_check) {
                                echo 'Bu email adresi ile daha önceden bir kayıt yapılmış';
                            } else {
                                $result = $staff->newStaff($staff_data);
                                if ($result > 0) {
                                    echo 'İşlem Başarılı';
                                } else {
                                    echo 'İşlem Başarısız';
                                }
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'duzenle':
                        if ($is_login and $role) {
                            $id = $route->getId();
                            $staff_data = $staff->getStaffInfo($id);
                            $permission_list = $staff->getPermissionList($id);
                            $module_list = $modules->getModuleList();

                            foreach ($module_list as $module) {
                                foreach ($permission_list as $perm) {

                                    $find = array_search($module['ID'], $perm);
                                    if ($find) {
                                        $sil = true;
                                    }
                                }
                                if (!$sil) {
                                    $no_perms[] = $module;
                                }
                                $sil = false;
                            }
                            $tpl->assign('staff_data', $staff_data);
                            $tpl->assign('module_list', $no_perms);
                            $tpl->assign('permission_list', $permission_list);
                            $html = $tpl->draw('staff-update', $return_string = true);
                            echo $html;
                        } else {
                            header("Location: https://panel.stokizle.com");
                        }
                        break;
                    case 'guncelle':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $staff_data[$key] = removeXSS($val);
                            }
                            $staff_id = $staff_data['ID'];

                            $staff_data['SIFRE'] = md5($staff_data['SIFRE']);
                            unset($staff_data['ID']);
                            $result = $staff->updateStaffInfo($staff_id, $staff_data);
                            if ($result) {
                                echo 'İşlem Başarılı olmuştur.';
                            } else {
                                echo 'İşlem Başarısız olmuştur.';
                            }
                        } else {
                            echo 'Yetkisiz İşlem';
                        }
                        break;
                    case 'yetkisil':
                        if ($is_login and $role) {
                            $perm_id = $route->getId();
                            $result = $staff->deletePermission($perm_id);
                            if ($result > 0) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız';
                            }
                        } else {
                            echo 'Yetkisiz İşlem';
                        }
                        break;
                    case 'yetkiekle':
                        if ($is_login and $role) {
                            $staffId = $route->getId();
                            $perm_id = $_POST['module'];
                            $get_perm = $staff->checkPermission($staffId, $perm_id);
                            if ($get_perm) {
                                echo 'Bu yetki daha önce tanımlanmıştır.';
                            } else {
                                $result = $staff->newPermission($staffId, $perm_id);
                                if ($result > 0) {
                                    echo 'Yetki Eklendi';
                                } else {
                                    echo 'Yetki Ekleme Başarısız Oldu';
                                }
                            }
                        } else {
                            echo 'Yetkisiz İşlem';
                        }
                        break;
                }
                break;
            case 'ayarlar':
                $modLogin = new \modules\modLogin();
                switch ($route->getSub()) {

                    // Ayarlar->Porfil
                    case 'avataryukle':
                        if ($is_login) {
                            $file = $_FILES;
                            $tmp_file = $file['files']['tmp_name'][0];
                            $file_name = $file['files']['name'][0];
                            $extention = explode('.', $file_name);
                            $extention = end($extention);
                            $new_file = stok\Setting::AVATAR_PATH . '/' . $staff_info->ID . '.' . $extention;
                            $thumb_file = stok\Setting::AVATAR_PATH . '/' . $staff_info->ID . '_45.' . $extention;
                            move_uploaded_file($tmp_file, $new_file);

                            if (file_exists($new_file)) {
                                $thumb = new Imagick();
                                $thumb->readImage($new_file);
                                $thumb->resizeImage(45, 45, Imagick::FILTER_LANCZOS, 1);
                                $thumb->writeImage($thumb_file);
                                $thumb->clear();
                                $thumb->destroy();
                                echo "İşlem Başarılı olmuştur";
                            } else {
                                echo 'İşlem Başarısız Olmuştur';
                            }
                        }
                        break;
                    case 'session':
                        var_dump($_SESSION);
                        break;
                    case 'profil':
                        if ($is_login and $role) {
                            $js_files[] = 'https://www.stokizle.com/temalar/backend/js/form_control.js';
                            $js_files[] = '//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js';
                            $tpl->assign('reminders', $staff->getReminders($staff_info->ID, 5));
                            $tpl->assign('js_import', '1');
                            $tpl->assign('js_files', $js_files);
                            $tpl->assign('name', $staff_info->ISIM);
                            $tpl->assign('phone', $staff_profile->TELEFON);
                            $html = $tpl->draw('profil', $return_string = true);
                        } else {
                            $tpl->assign('message', 'Yanlış bir yerdesiniz...');
                            $tpl->assign('url', stok\Setting::BE_URL);
                            $tpl->assign('time', '3');
                            $html = $tpl->draw('notice', $return_string = true);
                        }
                        echo $html;
                        break;
                    case 'profilkaydet':
                        if ($is_login and $role) {
                            $id = $staff_info->ID;
                            foreach ($_POST as $key => $value) {
                                $data[$key] = removeXSS($value);
                            }
                            $staff_info_data['ISIM'] = $data['ISIM'];
                            $staff_profile_data['TELEFON'] = $data['TELEFON'];
                            $result_info = $staff->updateStaffInfo($id, $staff_info_data);
                            $result_profile = $staff->updateStaffProfile($id, $staff_profile_data);
                            if ($result_info > 0 or $result_profile > 0) {
                                $modLogin->refreshSession();
                                echo 'İşleminiz Başarılı olmuştur.';
                            } else {
                                echo 'İşleminiz Başarısız olmuştur.';
                            }
                        } else {
                            echo 'Yanlış bir yerdesiniz...';
                        }
                        break;
                    case 'sifredegistir':
                        if ($is_login) {
                            $id = $staff_info->ID;
                            $data['SIFRE'] = md5(removeXSS($_POST['new-password']));
                            $result = $staff->updateStaffInfo($id, $data);
                            if ($result > 0) {
                                $session = json_decode($_SESSION['info'], true);
                                $session['sifreyenile'] = 0;
                                $message = 'Şifreniz güncellendi.';
                            } else {
                                $message = 'Güncelleme Başarısız..';
                            }
                            echo $message;
                        }
                        break;
                    // Ayarlar->Sirket
                    case 'sirket':
                        if ($is_login and $role) {
                            $company_info = $company->getCompanyInfo($staff_info->SIRKET_ID);
                            $tpl->assign('company_info', $company_info);
                            $html = $tpl->draw('company-profile', $return_string = true);
                        } else {
                            $tpl->assign('message', 'Yetkisiz Alan.');
                            $tpl->assign('url', stok\Setting::BE_URL);
                            $tpl->assign('time', '3');
                            $html = $tpl->draw('notice', $return_string = true);
                        }
                        echo $html;
                        break;
                    case 'sirketayarlari':
                        if ($is_login and $role) {
                            $company_setting = $company->getCompanySetting($staff_info->SIRKET_ID);
                            $warehouses = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                            $tpl->assign('company_setting', $company_setting);
                            $tpl->assign('warehouses', $warehouses);
                            $html = $tpl->draw('company-setting', $return_string = true);
                            echo $html;
                        } else {
                            header("Location: https://panel.stokizle.com");
                        }
                        break;
                    case 'sirketayarkaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $val) {
                                $setting[$key] = removeXSS($val);
                            }
                            $result = $company->updateCompanySetting($staff_info->SIRKET_ID, $setting);
                            if ($result) {
                                echo 'İşlem Başarılı';
                            } else {
                                echo 'İşlem Başarısız';
                            }
                        } else {
                            echo 'Yetkisiz Alan';
                        }
                        break;
                    case 'sirketprofilkaydet':
                        if ($is_login and $role) {
                            foreach ($_POST as $key => $value) {
                                $data[$key] = removeXSS($value);
                            }

                            $result = $company->updateCompanyInfo($sirket_id, $data);
                            if ($result > 0) {

                                echo 'işlem Başarılı olmuştur';
                            } else {
                                echo 'işlem Başarısız olmuştur';
                            }
                        } else {
                            echo 'Yetkisiz alan';
                        }
                        break;
                    default:
                        header('Location: ' . stok\Setting::BE_URL);
                        break;
                }
                break;
            default:
                if (!$is_login) {
                    if ($_GET['msg']) {
                        $tpl->assign('message', $_GET['msg']);
                    } else {
                        $tpl->assign('message', 'Giriiş yapmak için bilgilerinizi giriniz...');
                    }
                    $html = $tpl->draw('login', $return_string = true);
                    echo $html;
                } else {
                    if($_SESSION['sirket_data']['DURUM']=='aktif'){
                        $warehouse = $warehouseCls->getWarehouses($staff_info->SIRKET_ID);
                        if (!$warehouse) {
                            $tpl->assign('warehouse', true);
                        }
                        $tpl->assign('id', $staff_info->ID);
                        if ($_SESSION['sirket_id'] > 0) {
                            $aktif_sirket_id = $_SESSION['sirket_id'];
                            $total_stocks = $rapor->getAllStockCount($staff_info->SIRKET_ID);
                            $tpl->assign('total_stocks', $total_stocks);
                            $html = $tpl->draw('index', $return_string = true);
                        } else {
                            $html = $tpl->draw('company-add', $return_string = true);
                        }
                        echo $html;
                    }elseif($_SESSION['sirket_data']['DURUM']=='odeme'){
                         $html = $tpl->draw('satinal', $return_string = true);
                         echo $html;
                    }
                }

                break;
        }
        break;

    default:
        echo 'Geçersiz Sayfa';
        break;
}
