<?php

require_once ('/srv/stokizle.com.prod/site/config.php');
require_once (stok\Setting::FW_PATH . "lib/general.lib.php");
require_once (stok\Setting::FW_PATH . "lib/memcache.lib.php");
require_once (stok\Setting::FW_PATH . "lib/mysql.lib.php");
require_once (stok\Setting::FW_PATH . "lib/phpmailer/class.phpmailer.php");
require_once (stok\Setting::FW_PATH . "lib/sms.lib.php");
require_once (stok\Setting::FW_PATH . "modules/db.mod.php");


require_once (stok\Setting::SITE_PATH . "class/payment.php");
require_once (stok\Setting::SITE_PATH . "class/mail.php");


// yeni sistem
require_once (stok\Setting::SITE_PATH . "class/staff.php");
require_once (stok\Setting::SITE_PATH . "class/modules.php");
require_once (stok\Setting::SITE_PATH . "class/company.php");
require_once (stok\Setting::SITE_PATH . "class/stock.php");
require_once (stok\Setting::SITE_PATH . "class/warehouse.php");
require_once (stok\Setting::SITE_PATH . "class/reports.php");
// onemli tanimlar


$database = new arte\Database(array(
    'user' => stok\Setting::MYSQL_USER,
        'password' => stok\Setting::MYSQL_PASSWORD,
            'host' => stok\Setting::MYSQL_HOST,
                'database' => stok\Setting::MYSQL_DATABASE
                    ));

$db = new arte\db();

$mailer = new PHPMailer();
$mail = new stok\Mail();

$stockCls = new stok\Stock();
$payment = new stok\Payment();

// yeni sistem
$modules = new stok\Modules();
$staff = new stok\Staff();
$company = new stok\Company();
$warehouseCls = new stok\warehouse();
$sms = new arte\sms();
$rapor = new stok\reports();


$now = date("Y-m-d H:i:s");
$now =new DateTime($now);
$db->table('SIRKET');
$db->select(array('ID','PAKET','DURUM','PAKET_BITIS_TARIHI','SIRKET_TELEFONU'));
$db->where(array('DURUM'=>'aktif'),'');
$db->get();
while($result = $db->result()){
    $data[]=$result;
}

foreach($data as $company){
    $end_date = new DateTime($company['PAKET_BITIS_TARIHI']);

    $date_diff = date_diff($now, $end_date);
    $date_diff=$date_diff->format('%R%a');
    echo $date_diff;
    if($date_diff<0){
        echo $date_diff;
        $db->table('SIRKET');
        $db->where(array('ID'=>$company['ID']),'');
        $db->update(array('DURUM'=>'pasif'));
    }

    if($date_diff>0 and $date_diff<=3){
        $data['mesaj']='Sotkizle.com Paketiniz 3 gün içerisinde bitecektir.';
        $data['receipents']=$company['SIRKET_TELEFONU'];
        $sms->sendSms(stok\Setting::$SMS,$data);

    }

}
