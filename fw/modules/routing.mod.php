<?php

/*
 * ozgurkuru november 2011
 */
namespace arte;

class Routes
{

    private $routing_data;
    private $master;
    private $sub;
    private $id;
    private $page;
    private $subdomain;

    public function __construct($routing_data)
    {

        $this->routing_data = $routing_data;
        $this->master = $this->routing_data["master"];
        $this->sub = $this->routing_data["sub"];
        $this->id = $this->routing_data["id"];
        $this->debug = $this->routing_data["debug"];
        $this->page = $this->routing_data['page'];
        $this->subdomain = $this->routing_data['subdomain'];
    }
    
    public function getMaster()
    {
        return $this->master;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSub()
    {
        return $this->sub;
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getSubdomain(){
        return $this->subdomain;
    }
    

}
