<?php

/*
 * ozgur kuru november 2011
 */

namespace arte;

class Routing
{

    private $request;
    private $request_domain;
    private $pieces;
    private $MasterPage;
    private $SubPage = false;
    private $SubPageValue;
    private $piece_count;
    private $routing_data;
    private $debug;
    private $page;
    public $id;

    public function __construct()
    {
        $this->request = $_SERVER["REQUEST_URI"];

        $this->pieces = explode("/", $this->request);
        if (count($this->pieces) > 2) {
            $this->SubPageValue = $this->pieces[2];
            $this->SubPage = true;
        }

        foreach ($this->pieces as $key => $value) {
            if (is_numeric($value)) {
                $this->id = $value;
                break;
            }
        }

        $debug = array_search("debug", $this->pieces);
        $page = array_search("sayfa", $this->pieces);
        if ($debug >= 1) {
            $this->debug = true;
        }
        if ($page) {
            $this->page = $this->id;
            $this->id = 0;
        }
        $this->MasterPage = $this->pieces[1];

        $this->piece_count = count($this->pieces);
    }

    public function router()
    {

        $this->routing_data = array(
            "master" => $this->MasterPage,
            "sub" => $this->SubPageValue,
            "id" => $this->id,
            "debug" => $this->debug,
            "page" => $this->page,
            "subdomain" => self::domainRouter()
        );
        return $this->routing_data;
    }

    private function domainRouter()
    {
        $this->request_domain = $_SERVER["HTTP_HOST"];

        $rdomain = explode(".", $this->request_domain);
        if (count($rdomain) > 2) {
            $subdomain = $rdomain[0];
        } else {
            $subdomain = 'www';
        }
        return $subdomain;
    }

}
