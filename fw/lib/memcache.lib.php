<?php

/**
 * @author Özgür Kuru
 * @since
 * @version
 */

namespace arte;

class Cache
{
    private $memcache;
    private $compress;

    public function __construct($memcacheInfo)
    {
        $this->memcache = new \Memcache();
        $this->compress= $memcacheInfo['compress'];
        foreach ($memcacheInfo['server'] as $host => $port) {
            $this->memcache->addServer($host, $port);
        }
    }

    public function add($data)
    {
        $result= $this->memcache->add(
            $data['key'],
            $data['value'],
            $this->compress,
            $data['timeout']
        );
    }

    public function get($key)
    {
        return $this->memcache->get($key);
    }

    public function delete($key)
    {
        return $this->memcache->delete($key);
    }
}
