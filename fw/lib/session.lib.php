<?php

namespace arte;

class Sessison
{
    private $user;
    private $userObj;

    public function __construct($userid)
    {
        global $userCls; # global user class object for gettin user info
        $this->userObj = $userCls;
        $this->user = $userid;
        session_start();

    }

    public function newSession($login_status)
    {
        if ($login_status) {
            $_SESSION["userid"]=$this->user;
            $_SESSION["username"] = $this->userObj->getUserName($this->user);
        }
    }

    public function sessionCheck()
    {
        if ($_ESSION["userid"]>0) {
                return true;
        } else {
                return false;
        }
    }

    public function sessionClose()
    {
        session_destroy();
    }
}
